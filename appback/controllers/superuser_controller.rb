class SuperuserController < ApplicationController
	layout "activescaffold"
	active_scaffold :member
before_filter :check_superuser

def check_superuser
      if session[:superuser] != 101
	
	redirect_to  "/login"
	end
end



def delcomm
	@tmpad = Mycomment.find(params[:id])
	if @tmpad
		@tmpad.destroy
	end
	redirect_to '/superuser/showcomments'
end




def apprcomm
	@tmpad = Mycomment.find(params[:id])
	if @tmpad
		@tmpad.status = 1
		@tmpad.update_attributes(:status => @tmpad.status)
		@myad = Ad.find(@tmpad.commentable_id)
		if @myad
			
			@mysub = "You have a new comment on your profile - Possible interest "
      	@mytext = Emailtext.find(:first, :conditions => "emailid='COMMAPP'")
		@mybody = @mytext.contents 
		Notifier::deliver_sendemail(@myad.member.email,@mysub,@mybody)

			@mysub = "Your Comment to " + Sitestuff::MYDOMAIN + " has been is approved"
      	@mytext = Emailtext.find(:first, :conditions => "emailid='COMMUSER'")
		@mybody = @mytext.contents + "<br/> Click here to view your comment  http://www." + Sitestuff::MYDOMAIN + "/myprofile/" + @myad.member_id.to_s
		Notifier::deliver_sendemail(@tmpad.email,@mysub,@mybody)
		end
		
	end
	redirect_to '/superuser/showcomments'
end



def showcomments
	@mycomms = Mycomment.find(:all, :conditions => 'status = 0 ', :order => 'id desc')
end




 def storynew
	    

	

    		@st = Mystory.new
  end
  
  
  def storysav
  	@st = Mystory.new(params[:st])
  	@st.save
  	
  	redirect_to '/superuser/storynew'
  	
  	end
  	
  	
  	
  	
def flexusers
@me = Member.find(:all, :order => 'id desc', :limit => 100)
render :xml => @me.to_xml

end
	  	
  	
  

def viewpms

 		@ad_pages, @ads = paginate(:pmessages, :conditions => "id > 0", :order => "id DESC  ", :per_page => 18)
                  


end

def madapprove
			@tmpad = Mad.find(params[:id])
		
		Mad.update_by_sql("update mads set status=1 where id = #{params[:id]}") 
		flash[:notice] = "Ad Approved"	

		@mysub = "Your Ad in HindusOnlyMatrimony.com has been Approved"
      	@mytext = Emailtext.find(:first, :conditions => "emailid='NEWAD'")
		@mybody = @mytext.contents
		Notifier::deliver_sendemail(@tmpad.member.email,@mysub,@mybody)
		redirect_to "/marketbrowse/all/all/all/all"
	end
	




def viewusers
        @query = params[:typed_search]
        @querysave = @query
        @mypage = params[:page]
        if @mypage 
        	@query = session[:myuserquery]
    	else
    		       @query = params[:typed_search]
    		       session[:myuserquery] = @query
		end
		
        @query = '%' + @query + '%'
 		@ad_pages, @ads = paginate(:members, :conditions => " id = " + params[:typed_search].to_i.to_s + " or email like '" + @query + 
 		"' or lname like '" + @query + "' or fname like '"  + @query +  "' or phone_no like '" + @query + 
 		 "' or ( mobile_no =" + 	@querysave.to_i.to_s + ' and ' +  @querysave.to_i.to_s + ' > 0)',
 		:order => "id DESC  ", :per_page => 18)
     
 		                  
        

end


def memberedit
end


def edituserprofile
	
	@myuser = Member.find(params[:id])
	session[:member] = @myuser.id
	redirect_to '/editmyprofile'
	
end

def index
end



#
def downgrade
	Member.update_by_sql("update members set usertype_id=1 where id= #{params[:id]}") 
	
@mypay = Memberpayment.new
@mypay.member_id = params[:id]
@mypay.exp_date = Date.today + 365
@mypay.save
flash[:notice] = 'User downgraded'
redirect_to request.env["HTTP_REFERER"] 
end



def emailverify
Member.update_by_sql("update members set emailverified=1 where id= #{params[:id]}") 
flash[:notice] = 'Email verified'
redirect_to request.env["HTTP_REFERER"] 
end


	

#
def paiduser
	Member.update_by_sql("update members set usertype_id=2 where id= #{params[:id]}") 
	@myuser = Member.find(:first, :conditions => 'id = ' + params[:id].to_s)

@tmpmy = Memberpayment.find(:first, :conditions => ' member_id = ' + @myuser.id.to_s )

if @tmpmy
   @tmpmy.exp_date = @tmpmy.exp_date.to_date + 365
   @tmpmy.update_attributes(:exp_date => @tmpmy.exp_date)
   flash[:notice] = 'User Extended'

   
else
	
@mypay = Memberpayment.new
@mypay.member_id = params[:id]
@mypay.exp_date = Date.today + 365
@mypay.save
flash[:notice] = 'User Upgraded'
end
redirect_to request.env["HTTP_REFERER"] 

end




#
def silverupgrade
	Member.update_by_sql("update members set usertype_id=2 where id= #{params[:id]}") 
	@myuser = Member.find(:first, :conditions => 'id = ' + params[:id].to_s)

@tmpmy = Memberpayment.find(:first, :conditions => ' member_id = ' + @myuser.id.to_s )

if @tmpmy
   @tmpmy.exp_date = @tmpmy.exp_date.to_date + 180
   @tmpmy.update_attributes(:exp_date => @tmpmy.exp_date)
   flash[:notice] = 'User Extended'

   
else
	
@mypay = Memberpayment.new
@mypay.member_id = params[:id]
@mypay.exp_date = Date.today + 180
@mypay.save
flash[:notice] = 'User Upgraded'
end
redirect_to request.env["HTTP_REFERER"] 

end



#
def bronzeupgrade
	Member.update_by_sql("update members set usertype_id=2 where id= #{params[:id]}") 
	@myuser = Member.find(:first, :conditions => 'id = ' + params[:id].to_s)

@tmpmy = Memberpayment.find(:first, :conditions => ' member_id = ' + @myuser.id.to_s )

if @tmpmy
   @tmpmy.exp_date = @tmpmy.exp_date.to_date + 90
   @tmpmy.update_attributes(:exp_date => @tmpmy.exp_date)
   flash[:notice] = 'User Extended'

   
else
	
@mypay = Memberpayment.new
@mypay.member_id = params[:id]
@mypay.exp_date = Date.today + 90
@mypay.save
flash[:notice] = 'User Upgraded'
end
redirect_to request.env["HTTP_REFERER"] 

end





def genderchange
	@myuser = Member.find(:first, :conditions => 'id = ' + params[:id].to_s)
	if @myuser.sex_id == 1 
		@myuser.sex_id =2
	else
		@myuser.sex_id=1
	end
	
	Member.update_by_sql("update members set sex_id=#{@myuser.sex_id} where id= #{params[:id]}") 
	Member.update_by_sql("update ads set adtype_id=#{@myuser.sex_id} where member_id= #{params[:id]}") 
	@myad = Ad.find(:first, :conditions => 'member_id = ' + @myuser.id.to_s)
	expire_fragment(:controller => 'rents101', :action => 'faq', :part => 'singlead'+@myad.id.to_s)
	 expire_fragment(:controller => 'rents101', :action => 'faq', :part => 'ad'+@myad.id.to_s)
	
flash[:notice] = 'User gender changed'
redirect_to request.env["HTTP_REFERER"] 
end



def clearimages
	Member.update_by_sql("update ads set image1=null, image2=null, image3=null, image4=null, image5=null, image6=null where id= #{params[:id]}") 
@myid = params[:id]
flash[:notice] = 'images cleared'
   expire_fragment(:controller => 'rents101', :action => 'faq', :part => 'ad'+@myid.to_s)
    expire_fragment(:controller => 'rents101', :action => 'faq', :part => 'singlead'+@myid.to_s)
redirect_to request.env["HTTP_REFERER"] 
end




#
def verified
	Member.update_by_sql("update ads set verifiedprofile=1 where id= #{params[:id]}") 

flash[:notice] = 'User marked as verified'
  expire_fragment(:controller => 'rents101', :action => 'faq', :part => 'ad'+params[:id].to_s)
    expire_fragment(:controller => 'rents101', :action => 'faq', :part => 'singlead'+params[:id].to_s)
redirect_to request.env["HTTP_REFERER"] 
end


def commentapprove

@tmp = Comment.find(params[:id])
@tmp.status = 1
@tmp.update_attributes(:status => @tmp.status)


expire_fragment(:controller => 'rents101', :action => 'faq', :part => 'ad'+@tmp.commentable_id.to_s)


redirect_to "/superuser/approvecomments"

end


def commentdestroy
Comment.find(params[:id]).destroy

redirect_to "/superuser/approvecomments"


end


#



end


