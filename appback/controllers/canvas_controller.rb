class CanvasController < ApplicationController
  
  before_filter :require_facebook_login
  
  def index
    render_with_facebook_debug_panel    
  end
  
  def controllerextensions
  end
  
  def modelextensions
  end

  
end
