class AdsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @ad_pages, @ads = paginate :ads, :per_page => 10
  end

  def show
    @ad = Ad.find(params[:id])
  end

  def new
    @ad = Ad.new
  end

  def create
    @ad = Ad.new(params[:ad])
    if @ad.save
      flash[:notice] = 'Ad was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @ad = Ad.find(params[:id])
  end

  def update
    @ad = Ad.find(params[:id])
    if @ad.update_attributes(params[:ad])
      flash[:notice] = 'Ad was successfully updated.'
      redirect_to :action => 'show', :id => @ad
    else
      render :action => 'edit'
    end
  end

  def destroy
    Ad.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
