class TellfriendController < ApplicationController




  def new
    session[:noisy_image] = NoisyImage.new(3)
    session[:code] = session[:noisy_image].code
    @tellafriend = Tellafriend.new
  end

  def create
    typed_code = params[:typed_code]
    mysender = params[:sender]
    mytarget = params[:receiver]
    mymsg = params[:comments]
    mysub = "RoRDirectory.com Recommended by " 
    mybody = "Your friend has recommended our site with the following message "
    Notifier.deliver_sendemail(params[:tellafriend_receiver],'123','456')

    unless typed_code == session[:noisy_image].code
      redirect_to(:controller => "rordir", :action => "captchaerror") and return false
    end
    @tellafriend = Tellafriend.new(params[:tellafriend])
    if @tellafriend.save
      flash[:notice] = 'Tellafriend was successfully created.'
      redirect_to :controller => 'rordir', :action => 'home'
    else
      render :action => 'new'
    end
  end

 
end
