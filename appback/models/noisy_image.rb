class NoisyImage

require 'RMagick'
include Magick

attr_reader :code, :code_image
  Jiggle = 0
  Wobble = 0

def initialize(len)
  chars = ('a'..'z').to_a - ['a','e','i','o','u','l']
  code_array=[]
  1.upto(len) {code_array << chars[rand(chars.length)]}
  granite = Magick::ImageList.new('granite:')
  canvas = Magick::ImageList.new
  canvas.new_image(40*len, 55, Magick::TextureFill.new(granite))
  text = Magick::Draw.new
  text.font_family = 'arial'
  text.pointsize = 24
  cur = 2

  code_array.each{|c|
    rand(10) > 5 ? rot=rand(Wobble):rot= -rand(Wobble)
    rand(10) > 5 ? weight = NormalWeight : weight = BoldWeight
    text.annotate(canvas,0,0,cur,30+rand(Jiggle),c){
      self.rotation=rot
      self.font_weight = weight
      self.fill = 'darkred'
    }
    cur += 30
  }
  @code = code_array.to_s
  @code_image = canvas.to_blob{
    self.format="JPG" 
  }
end

end
