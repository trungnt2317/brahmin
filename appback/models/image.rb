Line 1 require ' RMagick'
-
- class Image < ActiveRecord::Base
-
5 DIRECTORY = ' public/uploaded_images'
- THUMB_MAX_SIZE = [125,125]
-
- after_save :process
- after_destroy :cleanup

def file_data=(file_data)
- @file_data = file_data
- write_attribute ' extension' ,
- file_data.original_filename.split(' .' ).last.downcase
15 end


def url
- path.sub(/^public/,' ' )
- end
20
- def thumbnail_url
- thumbnail_path.sub(/^public/,' ' )
- end
-
25 def path
- File.join(DIRECTORY, "#{self.id}-full.#{extension}")
- end
-
- def thumbnail_path
30 File.join(DIRECTORY, "#{self.id}-thumb.#{extension}")
- end

#######
- private
35 #######
-
- def process
- if @file_data
- create_directory
40 cleanup
- save_fullsize
- create_thumbnail
- @file_data = nil




end
45 end
-
- def save_fullsize
- File.open(path,' w' ) do |file|
- file.puts @file_data.read
50 end
- end
-
- def create_thumbnail
- img = Magick::Image.read(path).first
55 thumbnail = img.thumbnail(*THUMB_MAX_SIZE)
- thumbnail.write thumbnail_path
- end
-
- def create_directory
60 FileUtils.mkdir_p DIRECTORY
- end
-
- def cleanup
- Dir[File.join(DIRECTORY, "#{self.id}-*")].each do |filename|
65 File.unlink(filename) rescue nil
- end
- end
-
- end