class Ad < ActiveRecord::Base
require_dependency "search"
require 'RMagick'
include Magick

belongs_to :member
belongs_to :adcity
belongs_to :propertytype
belongs_to :adtype
has_many :pmessages


validates_presence_of :property_price, :property_address, :title, :description


        file_column :image1, :magick => { 
          :versions => { "thumb" => "50x50", "medium" => "640x480>" }
        }


        file_column :image2, :magick => { 
          :versions => { "thumb" => "50x50", "medium" => "640x480>" }
        }


        file_column :image3, :magick => { 
          :versions => { "thumb" => "50x50", "medium" => "640x480>" }
        }


searches_on :all


def  self.getadtypes(mycondition)
	find_by_sql ("select distinct adtypes.id as adtypeid, adtypename, count(*) as adtypecount from "+
	" ads, adtypes where ads.adtype_id = adtypes.id and #{mycondition} group by ads.adtype_id order by adtypename")
end


def  self.getadcities(mycondition)
	find_by_sql ("select distinct adcities.id as adcityid, adcityname, count(*) as adcitycount from "+
	" ads, adcities where ads.adcity_id = adcities.id and #{mycondition} group by ads.adcity_id order by adcityname")
end

def  self.getadpropertytypes(mycondition)
	find_by_sql ("select distinct propertytypes.id as propertytypeid, propertytypename, count(*)  as " +
	" propertytypecount from "+
	" ads, propertytypes where ads.propertytype_id = propertytypes.id and #{mycondition} group by ads.propertytype_id" +
	" order by propertytypename")
end


end
