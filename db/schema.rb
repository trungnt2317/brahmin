# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of ActiveRecord to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 1) do

  create_table "LocalizationCategories", :primary_key => "ID", :force => true do |t|
    t.string "Name", :default => "", :null => false
  end

  add_index "LocalizationCategories", ["Name"], :name => "Name", :unique => true

  create_table "LocalizationKeys", :primary_key => "ID", :force => true do |t|
    t.integer "IDCategory", :limit => 3, :default => 0,  :null => false
    t.string  "Key",                     :default => "", :null => false
  end

  add_index "LocalizationKeys", ["Key"], :name => "Key", :unique => true

# Could not dump table "Profiles" because of following StandardError
#   Unknown type 'set('Very_Fair','Fair','Wheatish','Brown')' for column 'Complexion'

# Could not dump table "Profilesold" because of following StandardError
#   Unknown type 'set('Very_Fair','Fair','Wheatish','Brown')' for column 'Complexion'

  create_table "adcities", :force => true do |t|
    t.string  "adcityname",     :limit => 96, :default => "", :null => false
    t.integer "relative_order", :limit => 4,  :default => 0,  :null => false
    t.integer "jbstate_id",     :limit => 4,  :default => 0,  :null => false
  end

  add_index "adcities", ["adcityname"], :name => "adcityname"
  add_index "adcities", ["jbstate_id"], :name => "jbstate_id"

  create_table "adcomplaints", :force => true do |t|
    t.integer  "ad_id",        :limit => 20, :null => false
    t.integer  "complaint_id", :limit => 20, :null => false
    t.datetime "created_on",                 :null => false
  end

  create_table "ads", :force => true do |t|
    t.integer  "member_id",                         :default => 0,    :null => false
    t.string   "title",                             :default => "",   :null => false
    t.text     "description",                       :default => "",   :null => false
    t.integer  "status",                            :default => 0,    :null => false
    t.integer  "adtype_id",                                           :null => false
    t.integer  "adcity_id",          :limit => 20,  :default => 0,    :null => false
    t.integer  "propertytype_id",    :limit => 20,                    :null => false
    t.datetime "created_on"
    t.datetime "updated_on"
    t.string   "myurl",              :limit => 100, :default => "",   :null => false
    t.integer  "idcopy",             :limit => 20,  :default => 0,    :null => false
    t.integer  "poorviham_id",       :limit => 20,  :default => 1,    :null => false
    t.integer  "salaryrange_id",     :limit => 20,  :default => 0,    :null => false
    t.integer  "gothra_id",          :limit => 20,  :default => 5,    :null => false
    t.integer  "subsect_id",         :limit => 20,  :default => 5,    :null => false
    t.integer  "birthstar_id",       :limit => 20,  :default => 5,    :null => false
    t.string   "padam",              :limit => 80,  :default => "",   :null => false
    t.string   "nadhi",              :limit => 180, :default => "",   :null => false
    t.string   "partnerdescription", :limit => 500, :default => "",   :null => false
    t.integer  "sevvai",             :limit => 4,   :default => 0,    :null => false
    t.integer  "mangla",             :limit => 4,   :default => 0,    :null => false
    t.integer  "kuja",               :limit => 4,   :default => 0,    :null => false
    t.integer  "complexion_id",      :limit => 20,  :default => 1,    :null => false
    t.integer  "maritalstatus_id",   :limit => 20,  :default => 5,    :null => false
    t.string   "realname",           :limit => 180, :default => "",   :null => false
    t.integer  "zodiac_id",          :limit => 20,  :default => 1715, :null => false
    t.integer  "mycountry_id",       :limit => 20,  :default => 100,  :null => false
    t.string   "mycity1",            :limit => 180, :default => "",   :null => false
    t.string   "zip",                :limit => 180, :default => "",   :null => false
    t.integer  "year_id",            :limit => 20,  :default => 32,   :null => false
    t.integer  "month_id",           :limit => 20,  :default => 1,    :null => false
    t.integer  "height_id",          :limit => 20,  :default => 1547, :null => false
    t.integer  "bodytype_id",        :limit => 20,  :default => 0,    :null => false
    t.string   "occupation",         :limit => 180, :default => "",   :null => false
    t.string   "jobtype",            :limit => 180, :default => "",   :null => false
    t.string   "jobtransfer",        :limit => 180, :default => "",   :null => false
    t.string   "visastatus",         :limit => 180, :default => "",   :null => false
    t.integer  "language_id",        :limit => 20,  :default => 101,  :null => false
    t.integer  "educationmajor_id",  :limit => 20,  :default => 0,    :null => false
    t.string   "degrees",            :limit => 180, :default => "",   :null => false
    t.string   "specialconditions",  :limit => 180, :default => "",   :null => false
    t.integer  "vegeterian",         :limit => 4,   :default => 0,    :null => false
    t.string   "homeaddress",        :limit => 180, :default => "",   :null => false
    t.integer  "agerange1_id",       :limit => 4,   :default => 1,    :null => false
    t.integer  "agerange2_id",       :limit => 4,   :default => 1,    :null => false
    t.string   "lookingjobtype",     :limit => 180, :default => "",   :null => false
    t.string   "lookingsubsect",     :limit => 540, :default => "",   :null => false
    t.string   "lookingcomplexion",  :limit => 540, :default => "",   :null => false
    t.string   "lookingbrahmintype", :limit => 540, :default => "",   :null => false
    t.string   "lookingorigin",      :limit => 540, :default => "",   :null => false
    t.integer  "lookingheight_id",   :limit => 4,   :default => 0,    :null => false
    t.integer  "lookingbodytype_id", :limit => 4,   :default => 0,    :null => false
    t.string   "privatepassword",    :limit => 90
    t.integer  "picflag",                           :default => 0,    :null => false
    t.integer  "picaddon",                          :default => 0,    :null => false
    t.integer  "pic1",                              :default => 0,    :null => false
    t.integer  "pic2",                              :default => 0,    :null => false
    t.integer  "pic3",                              :default => 0,    :null => false
    t.integer  "pic4",                              :default => 0,    :null => false
    t.integer  "pic5",                              :default => 0,    :null => false
    t.string   "image1",             :limit => 200
    t.string   "image2",             :limit => 200
    t.string   "image3",             :limit => 200
    t.string   "image4",             :limit => 200
    t.string   "image5",             :limit => 200
    t.string   "image6",             :limit => 200
    t.integer  "teetotaler",         :limit => 4,   :default => 0,    :null => false
    t.integer  "sai",                :limit => 4,   :default => 0,    :null => false
    t.integer  "shirdi",             :limit => 4,   :default => 0,    :null => false
    t.string   "brahmintype",        :limit => 180, :default => "",   :null => false
    t.integer  "view",               :limit => 20,  :default => 0,    :null => false
    t.integer  "day_id",             :limit => 4,   :default => 1,    :null => false
    t.integer  "verifiedprofile",    :limit => 4,   :default => 0,    :null => false
    t.integer  "dontsmoke",          :limit => 4,   :default => 0,    :null => false
    t.integer  "bloodgroup_id",      :limit => 4,   :default => 9,    :null => false
    t.integer  "kanchi",             :limit => 4,   :default => 0,    :null => false
  end

  add_index "ads", ["member_id"], :name => "employer_id"
  add_index "ads", ["adtype_id"], :name => "adtype_id"
  add_index "ads", ["adcity_id"], :name => "adcity_id"
  add_index "ads", ["propertytype_id"], :name => "propertytype_id"

  create_table "adsbackup", :force => true do |t|
    t.integer  "member_id",                         :default => 0,    :null => false
    t.string   "title",                             :default => "",   :null => false
    t.text     "description",                       :default => "",   :null => false
    t.integer  "status",                            :default => 0,    :null => false
    t.integer  "adtype_id",                         :default => 0,    :null => false
    t.integer  "adcity_id",          :limit => 20,  :default => 0,    :null => false
    t.integer  "propertytype_id",    :limit => 20,  :default => 0,    :null => false
    t.datetime "created_on"
    t.datetime "updated_on"
    t.string   "myurl",              :limit => 100, :default => "",   :null => false
    t.integer  "idcopy",             :limit => 20,  :default => 0,    :null => false
    t.integer  "poorviham_id",       :limit => 20,  :default => 1,    :null => false
    t.integer  "salaryrange_id",     :limit => 20,  :default => 0,    :null => false
    t.integer  "gothra_id",          :limit => 20,  :default => 5,    :null => false
    t.integer  "subsect_id",         :limit => 20,  :default => 5,    :null => false
    t.integer  "birthstar_id",       :limit => 20,  :default => 5,    :null => false
    t.string   "padam",              :limit => 80,  :default => "",   :null => false
    t.string   "nadhi",              :limit => 180, :default => "",   :null => false
    t.string   "partnerdescription", :limit => 500, :default => "",   :null => false
    t.integer  "sevvai",             :limit => 4,   :default => 0,    :null => false
    t.integer  "mangla",             :limit => 4,   :default => 0,    :null => false
    t.integer  "kuja",               :limit => 4,   :default => 0,    :null => false
    t.integer  "complexion_id",      :limit => 20,  :default => 1,    :null => false
    t.integer  "maritalstatus_id",   :limit => 20,  :default => 5,    :null => false
    t.string   "realname",           :limit => 180, :default => "",   :null => false
    t.integer  "zodiac_id",          :limit => 20,  :default => 1715, :null => false
    t.integer  "mycountry_id",       :limit => 20,  :default => 100,  :null => false
    t.string   "mycity1",            :limit => 180, :default => "",   :null => false
    t.string   "zip",                :limit => 180, :default => "",   :null => false
    t.integer  "year_id",            :limit => 20,  :default => 32,   :null => false
    t.integer  "month_id",           :limit => 20,  :default => 1,    :null => false
    t.integer  "height_id",          :limit => 20,  :default => 1547, :null => false
    t.integer  "bodytype_id",        :limit => 20,  :default => 0,    :null => false
    t.string   "occupation",         :limit => 180, :default => "",   :null => false
    t.string   "jobtype",            :limit => 180, :default => "",   :null => false
    t.string   "jobtransfer",        :limit => 180, :default => "",   :null => false
    t.string   "visastatus",         :limit => 180, :default => "",   :null => false
    t.integer  "language_id",        :limit => 20,  :default => 101,  :null => false
    t.integer  "educationmajor_id",  :limit => 20,  :default => 0,    :null => false
    t.string   "degrees",            :limit => 180, :default => "",   :null => false
    t.string   "specialconditions",  :limit => 180, :default => "",   :null => false
    t.integer  "vegeterian",         :limit => 4,   :default => 0,    :null => false
    t.string   "homeaddress",        :limit => 180, :default => "",   :null => false
    t.integer  "agerange1_id",       :limit => 4,   :default => 1,    :null => false
    t.integer  "agerange2_id",       :limit => 4,   :default => 1,    :null => false
    t.string   "lookingjobtype",     :limit => 180, :default => "",   :null => false
    t.string   "lookingsubsect",     :limit => 540, :default => "",   :null => false
    t.string   "lookingcomplexion",  :limit => 540, :default => "",   :null => false
    t.string   "lookingbrahmintype", :limit => 540, :default => "",   :null => false
    t.string   "lookingorigin",      :limit => 540, :default => "",   :null => false
    t.integer  "lookingheight_id",   :limit => 4,   :default => 0,    :null => false
    t.integer  "lookingbodytype_id", :limit => 4,   :default => 0,    :null => false
    t.string   "privatepassword",    :limit => 90
    t.integer  "picflag",                           :default => 0,    :null => false
    t.integer  "picaddon",                          :default => 0,    :null => false
    t.integer  "pic1",                              :default => 0,    :null => false
    t.integer  "pic2",                              :default => 0,    :null => false
    t.integer  "pic3",                              :default => 0,    :null => false
    t.integer  "pic4",                              :default => 0,    :null => false
    t.integer  "pic5",                              :default => 0,    :null => false
    t.string   "image1",             :limit => 200
    t.string   "image2",             :limit => 200
    t.string   "image3",             :limit => 200
    t.string   "image4",             :limit => 200
    t.string   "image5",             :limit => 200
    t.string   "image6",             :limit => 200
    t.integer  "teetotaler",         :limit => 4,   :default => 0,    :null => false
    t.integer  "sai",                :limit => 4,   :default => 0,    :null => false
    t.integer  "shirdi",             :limit => 4,   :default => 0,    :null => false
    t.string   "brahmintype",        :limit => 180, :default => "",   :null => false
    t.integer  "view",               :limit => 20,  :default => 0,    :null => false
    t.integer  "day_id",             :limit => 4,   :default => 1,    :null => false
    t.integer  "verifiedprofile",    :limit => 4,   :default => 0,    :null => false
    t.integer  "dontsmoke",          :limit => 4,   :default => 0,    :null => false
  end

  create_table "adshistoryold", :force => true do |t|
    t.integer  "member_id",                         :default => 0,    :null => false
    t.string   "title",                             :default => "",   :null => false
    t.text     "description",                       :default => "",   :null => false
    t.integer  "status",                            :default => 0,    :null => false
    t.integer  "adtype_id",                         :default => 0,    :null => false
    t.integer  "adcity_id",          :limit => 20,  :default => 0,    :null => false
    t.integer  "propertytype_id",    :limit => 20,  :default => 0,    :null => false
    t.datetime "created_on"
    t.datetime "updated_on"
    t.string   "myurl",              :limit => 100, :default => "",   :null => false
    t.integer  "idcopy",             :limit => 20,  :default => 0,    :null => false
    t.integer  "poorviham_id",       :limit => 20,  :default => 1,    :null => false
    t.integer  "salaryrange_id",     :limit => 20,  :default => 0,    :null => false
    t.integer  "gothra_id",          :limit => 20,  :default => 5,    :null => false
    t.integer  "subsect_id",         :limit => 20,  :default => 5,    :null => false
    t.integer  "birthstar_id",       :limit => 20,  :default => 5,    :null => false
    t.string   "padam",              :limit => 80,  :default => "",   :null => false
    t.string   "nadhi",              :limit => 180, :default => "",   :null => false
    t.string   "partnerdescription", :limit => 500, :default => "",   :null => false
    t.integer  "sevvai",             :limit => 4,   :default => 0,    :null => false
    t.integer  "mangla",             :limit => 4,   :default => 0,    :null => false
    t.integer  "kuja",               :limit => 4,   :default => 0,    :null => false
    t.integer  "complexion_id",      :limit => 20,  :default => 1,    :null => false
    t.integer  "maritalstatus_id",   :limit => 20,  :default => 5,    :null => false
    t.string   "realname",           :limit => 180, :default => "",   :null => false
    t.integer  "zodiac_id",          :limit => 20,  :default => 1715, :null => false
    t.integer  "mycountry_id",       :limit => 20,  :default => 100,  :null => false
    t.string   "mycity1",            :limit => 180, :default => "",   :null => false
    t.string   "zip",                :limit => 180, :default => "",   :null => false
    t.integer  "year_id",            :limit => 20,  :default => 32,   :null => false
    t.integer  "month_id",           :limit => 20,  :default => 1,    :null => false
    t.integer  "height_id",          :limit => 20,  :default => 1547, :null => false
    t.integer  "bodytype_id",        :limit => 20,  :default => 0,    :null => false
    t.string   "occupation",         :limit => 180, :default => "",   :null => false
    t.string   "jobtype",            :limit => 180, :default => "",   :null => false
    t.string   "jobtransfer",        :limit => 180, :default => "",   :null => false
    t.string   "visastatus",         :limit => 180, :default => "",   :null => false
    t.integer  "language_id",        :limit => 20,  :default => 0,    :null => false
    t.integer  "educationmajor_id",  :limit => 20,  :default => 0,    :null => false
    t.string   "degrees",            :limit => 180, :default => "",   :null => false
    t.string   "specialconditions",  :limit => 180, :default => "",   :null => false
    t.integer  "vegeterian",         :limit => 4,   :default => 0,    :null => false
    t.string   "homeaddress",        :limit => 180, :default => "",   :null => false
    t.integer  "agerange1_id",       :limit => 4,   :default => 1,    :null => false
    t.integer  "agerange2_id",       :limit => 4,   :default => 1,    :null => false
    t.string   "lookingjobtype",     :limit => 180, :default => "",   :null => false
    t.string   "lookingsubsect",     :limit => 540, :default => "",   :null => false
    t.string   "lookingcomplexion",  :limit => 540, :default => "",   :null => false
    t.string   "lookingbrahmintype", :limit => 540, :default => "",   :null => false
    t.string   "lookingorigin",      :limit => 540, :default => "",   :null => false
    t.integer  "lookingheight_id",   :limit => 4,   :default => 0,    :null => false
    t.integer  "lookingbodytype_id", :limit => 4,   :default => 0,    :null => false
    t.string   "privatepassword",    :limit => 90
    t.integer  "picflag",                           :default => 0,    :null => false
    t.integer  "picaddon",                          :default => 0,    :null => false
    t.integer  "pic1",                              :default => 0,    :null => false
    t.integer  "pic2",                              :default => 0,    :null => false
    t.integer  "pic3",                              :default => 0,    :null => false
    t.integer  "pic4",                              :default => 0,    :null => false
    t.integer  "pic5",                              :default => 0,    :null => false
    t.string   "image1",             :limit => 200
    t.string   "image2",             :limit => 200
    t.string   "image3",             :limit => 200
    t.string   "image4",             :limit => 200
    t.string   "image5",             :limit => 200
    t.string   "image6",             :limit => 200
    t.integer  "teetotaler",         :limit => 4,   :default => 0,    :null => false
    t.integer  "sai",                :limit => 4,   :default => 0,    :null => false
    t.integer  "shirdi",             :limit => 4,   :default => 0,    :null => false
    t.string   "brahmintype",        :limit => 180, :default => "",   :null => false
    t.integer  "view",               :limit => 20,  :default => 0,    :null => false
    t.integer  "day_id",             :limit => 4,   :default => 1,    :null => false
  end

  create_table "adtypes", :force => true do |t|
    t.string "description", :limit => 100, :default => "", :null => false
    t.string "adtypename",  :limit => 60,  :default => "", :null => false
    t.string "url",         :limit => 20,  :default => "", :null => false
  end

  create_table "affiliates", :force => true do |t|
    t.string "username", :limit => 60,  :default => "", :null => false
    t.string "password", :limit => 60,  :default => "", :null => false
    t.string "email",    :limit => 100, :default => "", :null => false
  end

  add_index "affiliates", ["username", "email"], :name => "username", :unique => true

  create_table "agerange1s", :force => true do |t|
    t.integer "age", :limit => 4, :null => false
  end

  add_index "agerange1s", ["age"], :name => "age", :unique => true

  create_table "agerange2s", :force => true do |t|
    t.integer "age", :limit => 4, :null => false
  end

  add_index "agerange2s", ["age"], :name => "age", :unique => true

  create_table "birthstars", :force => true do |t|
    t.string "name", :limit => 30, :default => "", :null => false
  end

  add_index "birthstars", ["name"], :name => "name", :unique => true

  create_table "bloodgroups", :force => true do |t|
    t.string "name", :limit => 100, :default => "", :null => false
  end

  create_table "bodytypes", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  add_index "bodytypes", ["name"], :name => "name", :unique => true

  create_table "comments", :force => true do |t|
    t.string   "title",            :limit => 50,  :default => "", :null => false
    t.string   "comment",          :limit => 180, :default => "", :null => false
    t.datetime "created_at",                                      :null => false
    t.integer  "commentable_id",   :limit => 20,                  :null => false
    t.string   "commentable_type", :limit => 15,  :default => "", :null => false
    t.integer  "member_id",        :limit => 20
    t.integer  "status",           :limit => 4,   :default => 0,  :null => false
    t.string   "ipaddress",        :limit => 50,  :default => "", :null => false
  end

  create_table "complaints", :force => true do |t|
    t.string "name", :limit => 200, :default => "", :null => false
  end

  create_table "complexions", :force => true do |t|
    t.string "name", :limit => 30, :default => "", :null => false
  end

  add_index "complexions", ["name"], :name => "name", :unique => true

  create_table "credittransactions", :force => true do |t|
    t.integer  "credittype_id", :limit => 20,  :null => false
    t.integer  "member_id",     :limit => 20,  :null => false
    t.integer  "noofcredits",   :limit => 20,  :null => false
    t.string   "notes",         :limit => 180
    t.datetime "created_on",                   :null => false
  end

  create_table "credittypes", :force => true do |t|
    t.string "name",  :limit => 160, :default => "", :null => false
    t.string "descr", :limit => 180, :default => "", :null => false
  end

  create_table "days", :force => true do |t|
    t.integer "name", :limit => 4, :null => false
  end

  add_index "days", ["name"], :name => "name", :unique => true

  create_table "degrees", :force => true do |t|
    t.string "name", :limit => 30, :default => "", :null => false
  end

  add_index "degrees", ["name"], :name => "name", :unique => true

  create_table "educationmajors", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  add_index "educationmajors", ["name"], :name => "name", :unique => true

  create_table "educationtypes", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  create_table "emailrestracks", :force => true do |t|
    t.integer "adid", :limit => 20, :null => false
  end

  create_table "emailtexts", :force => true do |t|
    t.string "emailid",  :limit => 50, :default => "", :null => false
    t.text   "contents",               :default => "", :null => false
  end

  create_table "emailtracks", :force => true do |t|
    t.integer "adid", :limit => 20, :null => false
  end

  create_table "experiencelevels", :force => true do |t|
    t.string "name", :limit => 150, :default => "", :null => false
  end

  create_table "favorites", :force => true do |t|
    t.integer  "member_id",  :limit => 20, :default => 0, :null => false
    t.text     "url"
    t.datetime "created_on"
  end

  create_table "feedbacks", :force => true do |t|
    t.datetime "created_on",                    :null => false
    t.string   "email_id",       :limit => 100
    t.string   "msgtitle",       :limit => 100
    t.string   "msgdescription", :limit => 80
    t.string   "ipaddress",      :limit => 100
  end

  create_table "gothras", :force => true do |t|
    t.string "name", :limit => 30, :default => "", :null => false
  end

  add_index "gothras", ["name"], :name => "name", :unique => true

  create_table "heights", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  add_index "heights", ["name"], :name => "name", :unique => true

  create_table "jbcountries", :force => true do |t|
    t.string  "name",           :limit => 48,  :default => "", :null => false
    t.integer "relative_order", :limit => 4,   :default => 0,  :null => false
    t.string  "countryflag",    :limit => 180
    t.string  "currency",       :limit => 80,  :default => "", :null => false
  end

  add_index "jbcountries", ["name"], :name => "name", :unique => true

  create_table "jbemployers", :force => true do |t|
    t.integer "member_id",                    :default => 0,  :null => false
    t.string  "logo",          :limit => 180
    t.boolean "dont_apply"
    t.string  "company_name",  :limit => 160
    t.string  "main_phone",    :limit => 60
    t.string  "url",           :limit => 200
    t.integer "adcity_id",     :limit => 20,                  :null => false
    t.integer "jbcountry_id",  :limit => 20
    t.integer "jbstate_id",    :limit => 20
    t.string  "address1",      :limit => 70
    t.string  "address2",      :limit => 70
    t.string  "profile",       :limit => 900
    t.string  "small_logo",    :limit => 180
    t.string  "zip",           :limit => 20,  :default => "", :null => false
    t.string  "urlname",       :limit => 180
    t.integer "resumecredits", :limit => 20,  :default => 0,  :null => false
    t.integer "resumedebits",  :limit => 20,  :default => 0,  :null => false
  end

  create_table "jbresumes", :force => true do |t|
    t.integer  "member_id",                   :default => 0,    :null => false
    t.string   "name",         :limit => 64,  :default => "",   :null => false
    t.string   "description",  :limit => 400, :default => "",   :null => false
    t.text     "resumetext",                  :default => "",   :null => false
    t.integer  "address_1",                   :default => 0,    :null => false
    t.integer  "address_2",                   :default => 0,    :null => false
    t.integer  "phone_1",                     :default => 0,    :null => false
    t.integer  "phone_2",                     :default => 0,    :null => false
    t.integer  "email_1",                     :default => 0,    :null => false
    t.integer  "email_2",                     :default => 0,    :null => false
    t.integer  "template_id",                 :default => 0,    :null => false
    t.boolean  "incomplete",                  :default => true, :null => false
    t.integer  "resumeactive", :limit => 20,  :default => 1,    :null => false
    t.string   "mypitch",      :limit => 400
    t.datetime "created_on",                                    :null => false
  end

  add_index "jbresumes", ["member_id", "name"], :name => "uniq1", :unique => true
  add_index "jbresumes", ["member_id"], :name => "seeker_id"

  create_table "jbstates", :force => true do |t|
    t.integer "jbcountry_id",   :limit => 4,   :default => 1,  :null => false
    t.string  "name",           :limit => 128, :default => "", :null => false
    t.integer "relative_order",                :default => 0,  :null => false
    t.string  "statecode",      :limit => 60
  end

  add_index "jbstates", ["jbcountry_id"], :name => "parent_id"

  create_table "jobapplications", :force => true do |t|
    t.integer  "member_id",    :limit => 20,                  :null => false
    t.integer  "ad_id",        :limit => 20,                  :null => false
    t.datetime "created_at",                                  :null => false
    t.string   "cover_letter", :limit => 300, :default => "", :null => false
    t.string   "myresume",     :limit => 600, :default => "", :null => false
  end

  add_index "jobapplications", ["member_id", "ad_id"], :name => "member_id", :unique => true

  create_table "jobtypes", :force => true do |t|
    t.string "name", :limit => 30, :default => "", :null => false
  end

  add_index "jobtypes", ["name"], :name => "name", :unique => true

  create_table "languages", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  add_index "languages", ["name"], :name => "name", :unique => true

  create_table "lastlogins", :force => true do |t|
    t.datetime "created_on",                                :null => false
    t.integer  "member_id",  :limit => 20,                  :null => false
    t.string   "ip_address", :limit => 180, :default => "", :null => false
  end

  create_table "lookingbodytypes", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  add_index "lookingbodytypes", ["name"], :name => "name", :unique => true

  create_table "lookingheights", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  add_index "lookingheights", ["name"], :name => "name", :unique => true

  create_table "majors", :force => true do |t|
    t.string "name", :limit => 80, :default => "", :null => false
  end

  create_table "maritalstatuses", :force => true do |t|
    t.string "name", :limit => 30, :default => "", :null => false
  end

  add_index "maritalstatuses", ["name"], :name => "name", :unique => true

  create_table "memberpayments", :force => true do |t|
    t.integer  "member_id",  :limit => 20,                                 :default => 0,  :null => false
    t.string   "username",   :limit => 45,                                 :default => "", :null => false
    t.datetime "created_on",                                                               :null => false
    t.integer  "amountpaid", :limit => 10,  :precision => 10, :scale => 0, :default => 1,  :null => false
    t.string   "notes",      :limit => 500,                                :default => "", :null => false
    t.datetime "exp_date",                                                                 :null => false
  end

  create_table "memberpayments_history", :force => true do |t|
    t.integer  "member_id",  :limit => 20,                                 :default => 0,  :null => false
    t.string   "username",   :limit => 45,                                 :default => "", :null => false
    t.datetime "created_on",                                                               :null => false
    t.integer  "amountpaid", :limit => 10,  :precision => 10, :scale => 0,                 :null => false
    t.string   "notes",      :limit => 500,                                :default => "", :null => false
    t.datetime "exp_date",                                                                 :null => false
  end

  create_table "members", :force => true do |t|
    t.string   "username",       :limit => 128, :default => "",       :null => false
    t.string   "password",       :limit => 32,  :default => "",       :null => false
    t.string   "status",         :limit => 8,   :default => "seeker", :null => false
    t.string   "salutation",     :limit => 32,  :default => "",       :null => false
    t.string   "fname",          :limit => 64,  :default => "",       :null => false
    t.string   "middle_initial", :limit => 1,   :default => "",       :null => false
    t.string   "lname",          :limit => 96,  :default => "",       :null => false
    t.string   "suffix",         :limit => 16,  :default => "",       :null => false
    t.string   "newsletter",     :limit => 3,   :default => "yes",    :null => false
    t.string   "email",          :limit => 100, :default => "",       :null => false
    t.string   "ipaddress",      :limit => 90
    t.datetime "created_on"
    t.integer  "usertype_id",    :limit => 20,  :default => 1,        :null => false
    t.integer  "superuser"
    t.string   "phone_no",       :limit => 50
    t.integer  "mobile_no",      :limit => 20
    t.string   "time_to_call",   :limit => 80
    t.integer  "membertype_id",  :limit => 20,  :default => 1,        :null => false
    t.string   "nickname",       :limit => 180, :default => "",       :null => false
    t.integer  "feepaid",        :limit => 4,   :default => 0,        :null => false
    t.integer  "sex_id",         :limit => 20,  :default => 1,        :null => false
    t.integer  "confirmcode",    :limit => 20,  :default => 0,        :null => false
    t.integer  "confirmed",      :limit => 2,   :default => 1,        :null => false
    t.string   "referredby",     :limit => 200
    t.string   "affname",        :limit => 100
  end

  add_index "members", ["username"], :name => "login", :unique => true
  add_index "members", ["nickname"], :name => "nickname", :unique => true

  create_table "membersave", :force => true do |t|
    t.string   "username",       :limit => 128, :default => "",       :null => false
    t.string   "password",       :limit => 32,  :default => "",       :null => false
    t.string   "status",         :limit => 8,   :default => "seeker", :null => false
    t.string   "salutation",     :limit => 32,  :default => "",       :null => false
    t.string   "fname",          :limit => 64,  :default => "",       :null => false
    t.string   "middle_initial", :limit => 1,   :default => "",       :null => false
    t.string   "lname",          :limit => 96,  :default => "",       :null => false
    t.string   "suffix",         :limit => 16,  :default => "",       :null => false
    t.string   "suffix_titles",  :limit => 64,  :default => "",       :null => false
    t.string   "company_name",   :limit => 128, :default => "",       :null => false
    t.integer  "signup_time",                   :default => 0,        :null => false
    t.boolean  "completed",                     :default => true,     :null => false
    t.string   "timezone",       :limit => 6,   :default => "0",      :null => false
    t.string   "use_dst",        :limit => 2,   :default => "no",     :null => false
    t.string   "language",       :limit => 3,   :default => "eng",    :null => false
    t.string   "expire_email",   :limit => 3,   :default => "yes",    :null => false
    t.string   "newsletter",     :limit => 3,   :default => "yes",    :null => false
    t.string   "email",          :limit => 100, :default => "",       :null => false
    t.string   "ipaddress",      :limit => 90
    t.datetime "created_on"
    t.integer  "usertype_id",    :limit => 20,  :default => 1,        :null => false
    t.integer  "superuser"
    t.string   "phone_no",       :limit => 50
    t.string   "mobile_no",      :limit => 50
    t.string   "time_to_call",   :limit => 80
    t.integer  "membertype_id",  :limit => 20,  :default => 1,        :null => false
    t.string   "nickname",       :limit => 180, :default => "",       :null => false
    t.integer  "feepaid",        :limit => 4,   :default => 0,        :null => false
    t.integer  "sex_id",         :limit => 20,  :default => 1,        :null => false
    t.integer  "confirmcode",    :limit => 20,  :default => 0,        :null => false
    t.integer  "confirmed",      :limit => 2,   :default => 1,        :null => false
  end

  create_table "membersold", :force => true do |t|
    t.string   "username",       :limit => 128, :default => "",       :null => false
    t.string   "password",       :limit => 32,  :default => "",       :null => false
    t.string   "status",         :limit => 8,   :default => "seeker", :null => false
    t.string   "salutation",     :limit => 32,  :default => "",       :null => false
    t.string   "fname",          :limit => 64,  :default => "",       :null => false
    t.string   "middle_initial", :limit => 1,   :default => "",       :null => false
    t.string   "lname",          :limit => 96,  :default => "",       :null => false
    t.string   "suffix",         :limit => 16,  :default => "",       :null => false
    t.string   "suffix_titles",  :limit => 64,  :default => "",       :null => false
    t.string   "company_name",   :limit => 128, :default => "",       :null => false
    t.integer  "signup_time",                   :default => 0,        :null => false
    t.boolean  "completed",                     :default => true,     :null => false
    t.string   "timezone",       :limit => 6,   :default => "0",      :null => false
    t.string   "use_dst",        :limit => 2,   :default => "no",     :null => false
    t.string   "language",       :limit => 3,   :default => "eng",    :null => false
    t.string   "expire_email",   :limit => 3,   :default => "yes",    :null => false
    t.string   "newsletter",     :limit => 3,   :default => "yes",    :null => false
    t.string   "email",          :limit => 100, :default => "",       :null => false
    t.string   "ipaddress",      :limit => 90
    t.datetime "created_on"
    t.integer  "usertype_id",    :limit => 20,  :default => 1,        :null => false
    t.integer  "superuser"
    t.string   "phone_no",       :limit => 50
    t.string   "mobile_no",      :limit => 50
    t.string   "time_to_call",   :limit => 80
    t.integer  "membertype_id",  :limit => 20,  :default => 1,        :null => false
    t.string   "nickname",       :limit => 180, :default => "",       :null => false
    t.integer  "feepaid",        :limit => 4,   :default => 0,        :null => false
    t.integer  "sex_id",         :limit => 20,  :default => 1,        :null => false
    t.integer  "confirmcode",    :limit => 20,  :default => 0,        :null => false
    t.integer  "confirmed",      :limit => 2,   :default => 1,        :null => false
  end

  create_table "membertypes", :force => true do |t|
    t.string  "name",        :limit => 100, :default => "", :null => false
    t.integer "adsallowed"
    t.string  "description", :limit => 200
    t.integer "broker",                     :default => 0
  end

  create_table "months", :force => true do |t|
    t.string "name", :limit => 30, :default => "", :null => false
  end

  create_table "mycountries", :force => true do |t|
    t.string  "name",         :limit => 180, :default => "", :null => false
    t.string  "iso2",         :limit => 2,   :default => "", :null => false
    t.string  "iso3",         :limit => 3,   :default => "", :null => false
    t.integer "isono",        :limit => 6,                   :null => false
    t.string  "region",       :limit => 90,  :default => "", :null => false
    t.string  "currency",     :limit => 90,  :default => "", :null => false
    t.string  "currencycode", :limit => 90,  :default => "", :null => false
  end

  add_index "mycountries", ["name"], :name => "name", :unique => true

  create_table "myresumes", :force => true do |t|
    t.integer  "member_id",   :limit => 20,                  :null => false
    t.integer  "jbresume_id", :limit => 20,                  :null => false
    t.datetime "created_on",                                 :null => false
    t.string   "ipaddress",   :limit => 90,  :default => "", :null => false
    t.string   "notes",       :limit => 200, :default => "", :null => false
    t.string   "tag1",        :limit => 90,  :default => "", :null => false
    t.string   "tag2",        :limit => 90,  :default => "", :null => false
    t.string   "tag3",        :limit => 90,  :default => "", :null => false
    t.string   "tag4",        :limit => 90,  :default => "", :null => false
    t.string   "tag5",        :limit => 90,  :default => "", :null => false
  end

  add_index "myresumes", ["member_id", "jbresume_id"], :name => "member_id", :unique => true

  create_table "mystats", :force => true do |t|
    t.date    "curdate",                :null => false
    t.integer "members",  :limit => 20, :null => false
    t.integer "profiles", :limit => 20, :null => false
    t.integer "paid",     :limit => 20, :null => false
  end

  create_table "old_profiles", :force => true do |t|
    t.integer  "member_id",                           :default => 0,   :null => false
    t.string   "recent_company",                      :default => "",  :null => false
    t.string   "recent_job_title",                    :default => "",  :null => false
    t.string   "start_month",          :limit => 32,  :default => "0", :null => false
    t.integer  "start_year",                          :default => 0,   :null => false
    t.string   "end_month",            :limit => 32,  :default => "0", :null => false
    t.integer  "end_year",                            :default => 0,   :null => false
    t.string   "school",               :limit => 64,  :default => "",  :null => false
    t.string   "major",                :limit => 64,  :default => "",  :null => false
    t.string   "recent_degree",        :limit => 32,  :default => "",  :null => false
    t.string   "job_category1",        :limit => 64,  :default => "",  :null => false
    t.string   "job_category2",        :limit => 64,  :default => "",  :null => false
    t.string   "position_type",        :limit => 64,  :default => "",  :null => false
    t.float    "min_salary",                          :default => 0.0, :null => false
    t.integer  "salarytype_id",        :limit => 32,  :default => 28,  :null => false
    t.integer  "relocation_id",        :limit => 20,  :default => 1,   :null => false
    t.string   "location1",            :limit => 64,  :default => "",  :null => false
    t.string   "location2",            :limit => 64,  :default => "",  :null => false
    t.string   "location3",            :limit => 64,  :default => "",  :null => false
    t.string   "exp_level",            :limit => 64,  :default => "",  :null => false
    t.string   "desired_travel",       :limit => 32,  :default => "",  :null => false
    t.string   "work_auth1",           :limit => 64,  :default => "",  :null => false
    t.string   "work_auth2",           :limit => 64,  :default => "",  :null => false
    t.string   "work_auth3",           :limit => 64,  :default => "",  :null => false
    t.string   "language1",            :limit => 64,  :default => "",  :null => false
    t.string   "language2",            :limit => 64,  :default => "",  :null => false
    t.string   "language3",            :limit => 64,  :default => "",  :null => false
    t.integer  "securityclearance_id", :limit => 20,  :default => 1,   :null => false
    t.string   "myphoto",              :limit => 180
    t.integer  "major_id",             :limit => 20,  :default => 49,  :null => false
    t.integer  "propertytype_id",      :limit => 20,  :default => 14,  :null => false
    t.integer  "positiontype_id",      :limit => 20,  :default => 21,  :null => false
    t.integer  "experiencelevel_id",   :limit => 20,  :default => 27,  :null => false
    t.integer  "travel_id",            :limit => 20,  :default => 57,  :null => false
    t.integer  "adcity_id",            :limit => 20,  :default => 0,   :null => false
    t.integer  "jbstate_id",           :limit => 20,  :default => 0,   :null => false
    t.integer  "jbcountry_id",         :limit => 20,  :default => 0,   :null => false
    t.string   "zip",                  :limit => 20
    t.string   "address1",             :limit => 101
    t.string   "address2",             :limit => 101
    t.integer  "educationtype_id",     :limit => 20
    t.datetime "created_on",                                           :null => false
    t.string   "donotsubmit",          :limit => 900
    t.string   "clearance_level",      :limit => 90
    t.string   "polygraph_level",      :limit => 90
  end

# Could not dump table "oldprofiles" because of following StandardError
#   Unknown type 'set('Very_Fair','Fair','Wheatish','Brown')' for column 'Complexion'

  create_table "params", :force => true do |t|
    t.integer "intvalue",  :limit => 20,                  :null => false
    t.string  "charvalue", :limit => 100, :default => "", :null => false
  end

  create_table "pmessages", :force => true do |t|
    t.integer  "from_user",       :limit => 20
    t.integer  "to_user",         :limit => 20
    t.string   "title",           :limit => 70
    t.text     "description"
    t.integer  "read_flag",                     :default => 0
    t.integer  "sender_delete",                 :default => 0
    t.integer  "receiver_delete",               :default => 0
    t.datetime "created_on"
    t.datetime "read_on"
    t.integer  "ad_id",           :limit => 20
  end

  create_table "poorvihams", :force => true do |t|
    t.string "name", :limit => 50, :default => "", :null => false
  end

  add_index "poorvihams", ["name"], :name => "name", :unique => true

  create_table "positiontypes", :force => true do |t|
    t.string "name", :limit => 70, :default => "", :null => false
    t.string "icon", :limit => 90
  end

  create_table "privacysettings", :force => true do |t|
    t.integer "member_id",     :default => 0,     :null => false
    t.boolean "show_name",     :default => false, :null => false
    t.boolean "show_address",  :default => false, :null => false
    t.boolean "show_email",    :default => false, :null => false
    t.boolean "show_location", :default => true,  :null => false
    t.boolean "show_phone",    :default => true,  :null => false
    t.boolean "show_fax",      :default => true,  :null => false
    t.boolean "show_profile",  :default => true,  :null => false
  end

  create_table "propertytypes", :force => true do |t|
    t.string "propertytypename", :limit => 100, :default => "", :null => false
    t.string "imgicon",          :limit => 180
  end

  create_table "ratings", :force => true do |t|
    t.integer  "rating",                                      :null => false
    t.datetime "created_at",                                  :null => false
    t.string   "rateable_type", :limit => 40, :default => "", :null => false
    t.integer  "rateable_id",   :limit => 20,                 :null => false
    t.integer  "member_id",     :limit => 20
  end

  add_index "ratings", ["rateable_id"], :name => "rateable_id"

  create_table "relocations", :force => true do |t|
    t.string "name", :limit => 18, :default => "", :null => false
  end

  create_table "rss_generator", :primary_key => "ID", :force => true do |t|
    t.string  "MysqlTable",                             :default => "",       :null => false
    t.string  "MysqlSelect",                            :default => "",       :null => false
    t.string  "MysqlFrom",                              :default => "",       :null => false
    t.string  "MysqlWhere",                             :default => "",       :null => false
    t.string  "MysqlOrderby",                           :default => "",       :null => false
    t.string  "MysqlOrder",                             :default => "",       :null => false
    t.string  "MysqlLimitStart",          :limit => 10, :default => "0",      :null => false
    t.string  "MysqlLimitEnd",            :limit => 10, :default => "15",     :null => false
    t.string  "Cache",                    :limit => 30, :default => "3600",   :null => false
    t.text    "FeedTitle",                              :default => "",       :null => false
    t.text    "URL",                                    :default => "",       :null => false
    t.text    "Description",                            :default => "",       :null => false
    t.string  "Language",                 :limit => 10, :default => "en-us",  :null => false
    t.text    "Copyright",                              :default => "",       :null => false
    t.string  "Webmaster",                              :default => "",       :null => false
    t.string  "Category",                               :default => "",       :null => false
    t.string  "Generator",                              :default => "",       :null => false
    t.string  "ItemTitle",                              :default => "",       :null => false
    t.string  "UseItemTitleTrim",         :limit => 1,  :default => "N",      :null => false
    t.string  "ItemTitleTrimChars",       :limit => 4,  :default => "",       :null => false
    t.string  "ItemTitleCont",            :limit => 15, :default => "",       :null => false
    t.string  "ItemDescription",                        :default => "",       :null => false
    t.string  "ItemDescriptionAllowHTML", :limit => 1,  :default => "N",      :null => false
    t.string  "UseItemDescriptionTrim",   :limit => 1,  :default => "N",      :null => false
    t.string  "ItemDescriptionTrimChars", :limit => 5,  :default => "",       :null => false
    t.string  "ItemDescriptionCont",      :limit => 15, :default => "",       :null => false
    t.string  "UID",                                    :default => "",       :null => false
    t.text    "GUID",                                   :default => "",       :null => false
    t.string  "PublishDate",                            :default => "",       :null => false
    t.string  "PublishDateFormat",                      :default => "RFC822", :null => false
    t.text    "Link",                                   :default => "",       :null => false
    t.string  "Author",                                 :default => "",       :null => false
    t.string  "ItemCategory",                           :default => "",       :null => false
    t.integer "LastBuild",                :limit => 32, :default => 0,        :null => false
  end

  create_table "rubyjobsurls", :force => true do |t|
    t.string "title",    :limit => 200, :default => "", :null => false
    t.string "location", :limit => 200, :default => "", :null => false
  end

  create_table "salaryranges", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  add_index "salaryranges", ["name"], :name => "name", :unique => true

  create_table "salarytypes", :force => true do |t|
    t.string "name", :limit => 100, :default => "", :null => false
  end

  create_table "savedressearches", :force => true do |t|
    t.integer  "propertytype_id",    :limit => 20
    t.integer  "experiencelevel_id", :limit => 20
    t.integer  "adcity_id",          :limit => 20
    t.integer  "member_id",          :limit => 20
    t.datetime "created_on"
    t.string   "searchterms",        :limit => 200
    t.string   "savedname",          :limit => 50
    t.integer  "emailalert",                        :default => 0
    t.datetime "lastemailsent"
    t.integer  "jbstate_id",         :limit => 20
    t.integer  "jbcountry_id",       :limit => 20
    t.integer  "security_clear",     :limit => 4,   :default => 0, :null => false
  end

  create_table "savedsearches", :force => true do |t|
    t.integer  "propertytype_id", :limit => 20
    t.integer  "adtype_id",       :limit => 20
    t.integer  "adcity_id",       :limit => 20
    t.integer  "member_id",       :limit => 20
    t.datetime "created_on"
    t.string   "searchterms",     :limit => 200
    t.string   "savedname",       :limit => 50
    t.integer  "emailalert",                     :default => 0
    t.datetime "lastemailsent"
    t.integer  "jbstate_id",      :limit => 20
    t.integer  "jbcountry_id",    :limit => 20
    t.integer  "agerange1_id",    :limit => 4,   :default => 1,  :null => false
    t.integer  "agerange2_id",    :limit => 4,   :default => 23, :null => false
  end

  create_table "securityclearances", :force => true do |t|
    t.string "name", :limit => 60, :default => "", :null => false
  end

  create_table "sessdelete", :force => true do |t|
  end

  create_table "sessions", :force => true do |t|
    t.string "sessid", :limit => 32
    t.text   "data"
  end

  add_index "sessions", ["sessid"], :name => "sessid"

  create_table "sexes", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  add_index "sexes", ["name"], :name => "name", :unique => true

  create_table "simple_captcha_data", :force => true do |t|
    t.string   "key",        :limit => 40
    t.string   "value",      :limit => 6
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sitesettings", :force => true do |t|
    t.integer "emailconf", :limit => 4, :null => false
  end

  create_table "subsects", :force => true do |t|
    t.string "name", :limit => 30, :default => "", :null => false
  end

  add_index "subsects", ["name"], :name => "name", :unique => true

  create_table "tellafriends", :force => true do |t|
    t.datetime "created_on",                                                           :null => false
    t.string   "sender",     :limit => 100, :default => "",                            :null => false
    t.string   "receiver",   :limit => 100, :default => "",                            :null => false
    t.string   "comments",   :limit => 400, :default => "",                            :null => false
    t.string   "url",        :limit => 200, :default => "http://www.RoRDirectory.com", :null => false
  end

  create_table "tmpsessions", :force => true do |t|
  end

  create_table "travels", :force => true do |t|
    t.string "name", :limit => 90, :default => "", :null => false
  end

  create_table "usertypes", :force => true do |t|
    t.string "name", :limit => 45, :default => "", :null => false
  end

  create_table "viewstats", :force => true do |t|
    t.integer "member_id",       :limit => 20, :null => false
    t.integer "viewedmember_id", :limit => 20, :null => false
    t.integer "mycounter",       :limit => 20, :null => false
  end

  add_index "viewstats", ["member_id", "viewedmember_id"], :name => "member_id", :unique => true

  create_table "wantedads", :force => true do |t|
    t.integer  "member_id",                                                          :default => 0,     :null => false
    t.string   "title",                                                              :default => "",    :null => false
    t.decimal  "approx_salary",                       :precision => 10, :scale => 2, :default => 0.0,   :null => false
    t.string   "short_description",                                                  :default => "",    :null => false
    t.text     "description",                                                        :default => "",    :null => false
    t.decimal  "pay_low",                             :precision => 10, :scale => 2, :default => 0.0,   :null => false
    t.decimal  "pay_high",                            :precision => 10, :scale => 2, :default => 0.0,   :null => false
    t.boolean  "step_completed",                                                     :default => false, :null => false
    t.integer  "view",                                                               :default => 0,     :null => false
    t.integer  "num_applicants",                                                     :default => 0,     :null => false
    t.integer  "time",                                                               :default => 0,     :null => false
    t.integer  "end_time",                                                           :default => 0,     :null => false
    t.integer  "service_type",                                                       :default => 0,     :null => false
    t.integer  "status",                                                             :default => 0,     :null => false
    t.integer  "adtype_id",                                                                             :null => false
    t.integer  "adcity_id",            :limit => 20,                                                    :null => false
    t.integer  "jbstate_id",           :limit => 20,                                                    :null => false
    t.integer  "jbcountry_id",         :limit => 20
    t.integer  "propertytype_id",      :limit => 20,                                                    :null => false
    t.datetime "created_on"
    t.datetime "updated_on"
    t.integer  "positiontype_id",      :limit => 20
    t.integer  "educationtype_id",     :limit => 20
    t.integer  "experiencelevel_id",   :limit => 20
    t.integer  "salarytype_id",        :limit => 20
    t.string   "job_code",             :limit => 200
    t.string   "external_url",         :limit => 200
    t.integer  "securityclearance_id", :limit => 20,                                 :default => 0
    t.string   "myurl",                :limit => 100,                                :default => "",    :null => false
    t.string   "job_requirement",      :limit => 240
    t.string   "key_skills",           :limit => 240
    t.string   "functional_area",      :limit => 120
    t.string   "job_role",             :limit => 120
    t.string   "contact_details",      :limit => 200
    t.integer  "telecommute",          :limit => 4,                                  :default => 0,     :null => false
    t.integer  "stock",                :limit => 4,                                  :default => 0,     :null => false
    t.integer  "retirement",           :limit => 4,                                  :default => 0,     :null => false
    t.integer  "vacation",             :limit => 4,                                  :default => 0,     :null => false
    t.integer  "sick",                 :limit => 4,                                  :default => 0,     :null => false
    t.integer  "daycare",              :limit => 4,                                  :default => 0,     :null => false
    t.integer  "health",               :limit => 4,                                  :default => 0,     :null => false
    t.integer  "dental",               :limit => 4,                                  :default => 0,     :null => false
    t.integer  "disability",           :limit => 4,                                  :default => 0,     :null => false
    t.integer  "tuition",              :limit => 4,                                  :default => 0,     :null => false
    t.integer  "zipcode",                                                            :default => 0,     :null => false
  end

  add_index "wantedads", ["member_id"], :name => "employer_id"
  add_index "wantedads", ["adtype_id"], :name => "adtype_id"
  add_index "wantedads", ["adcity_id"], :name => "adcity_id"
  add_index "wantedads", ["jbstate_id"], :name => "jbstate_id"
  add_index "wantedads", ["jbcountry_id"], :name => "jbcountry_id"
  add_index "wantedads", ["propertytype_id"], :name => "propertytype_id"
  add_index "wantedads", ["positiontype_id"], :name => "positiontype_id"
  add_index "wantedads", ["educationtype_id"], :name => "educationtype_id"
  add_index "wantedads", ["experiencelevel_id"], :name => "experiencelevel_id"
  add_index "wantedads", ["salarytype_id"], :name => "salarytype_id"
  add_index "wantedads", ["securityclearance_id"], :name => "securityclearance_id"

  create_table "workpermits", :force => true do |t|
    t.string "name", :limit => 30, :default => "", :null => false
  end

  add_index "workpermits", ["name"], :name => "name", :unique => true

  create_table "years", :force => true do |t|
    t.integer "name", :null => false
  end

  add_index "years", ["name"], :name => "name", :unique => true

  create_table "zodiacs", :force => true do |t|
    t.string "name", :limit => 180, :default => "", :null => false
  end

  add_index "zodiacs", ["name"], :name => "name", :unique => true

end
