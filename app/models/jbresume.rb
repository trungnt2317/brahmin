class Jbresume < ActiveRecord::Base

require_dependency "search"
require 'RMagick'
include Magick
acts_as_rateable
acts_as_ferret ( :fields => {
:name => {:store => :yes},
:description => {:store => :yes},
:resumetext => {:store => :yes},
:mypropertytype => {:store => :yes},
:mycity => {:store => :yes},
:mystate => {:store => :yes},
:mycountry => {:store => :yes}

})

def mypropertytype
  return "#{self.member.profile.propertytype.propertytypename}"
end

def mycity
  return "#{self.member.profile.adcity.adcityname}"
end


def mystate
  return "#{self.member.profile.adcity.jbstate.name}"
end

def mycountry
  return "#{self.member.profile.adcity.jbstate.jbcountry.name}"
end


def self.find_by_sql_with_limit(sql, offset, limit)
            sql = sanitize_sql(sql)
            add_limit!(sql, {:limit => limit, :offset => offset})
            find_by_sql(sql)
end

        def self.count_by_sql_wrapping_select_query(sql)
            sql = sanitize_sql(sql)
            count_by_sql("select count(*) from (#{sql}) as my_table")
        end



belongs_to :member
has_many :myresumes



def self.full_text_search(q, options = {})
   return nil if q.nil? or q==""
   default_options = {:limit => 18, :page => 1}
   options = default_options.merge options
   
   # get the offset based on what page we're on
   options[:offset] = options[:limit] * (options.delete(:page).to_i-1)  
   
   # now do the query with our options
   total_hits, results = Jbresume.find_storage_by_contents(q, options)
   return [total_hits, results]
end

def self.find_storage_by_contents(query, options = {})
  index = self.aaf_index.ferret_index # Get the index that acts_as_ferret created for us
  results = []
        
  # search_each is the core search function from Ferret, which Acts_as_ferret hides
  total_hits = index.search_each(query, options) do |id, score| 
    result = {}
        
    # Store each field in a hash which we can reference in our views
    result[:name] = index.highlight(query, id,
                    :field => :name, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:description] = index.highlight(query, id,
                    :field => :description, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:resumetext] = index.highlight(query, id,
                    :field => :resumetext, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:mystate] = index.highlight(query, id,
                    :field => :mystate, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:mycountry] = index.highlight(query, id,
                    :field => :mycountry, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:id] = index.highlight(query, id,
                    :field => :id, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:mypropertytype] = index.highlight(query, id,
                    :field => :mypropertytype, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:mycity] = index.highlight(query, id,
                    :field => :mycity, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)


    result[:score] = score   # We can even put the score in the hash, nice!
    result[:id] = index[id][:id]
    results.push result
  end
  return block_given? ? total_hits : [total_hits, results]
end


def  self.getjbstates(mycondition)
	find_by_sql ("select distinct jbstates.id as jbstateid, jbstates.name as jbstatename, count(*) as jbstatecount from "+
	" jbresumes, profiles, jbstates where jbresumes.member_id=profiles.member_id and profiles.jbstate_id = jbstates.id and #{mycondition} group by profiles.jbstate_id order by jbstatename")
end


def  self.getadcities(mycondition)
	find_by_sql ("select distinct adcities.id as adcityid, adcityname, count(*) as adcitycount from "+
	" jbresumes, profiles, adcities where jbresumes.member_id = profiles.member_id and profiles.adcity_id = adcities.id and #{mycondition} group by profiles.adcity_id order by adcityname")
end

def  self.getadpropertytypes(mycondition)
	find_by_sql ("select distinct propertytypes.id as propertytypeid, propertytypename, imgicon,  count(*)  as " +
	" propertytypecount from "+
	" jbresumes, profiles, propertytypes where jbresumes.member_id=profiles.member_id and profiles.propertytype_id = propertytypes.id and #{mycondition} group by profiles.propertytype_id" +
	" order by propertytypename")
end


        def self.find_by_sql_with_limit(sql, offset, limit)
            sql = sanitize_sql(sql)
            add_limit!(sql, {:limit => limit, :offset => offset})
            find_by_sql(sql)
        end

        def self.count_by_sql_wrapping_select_query(sql)
            sql = sanitize_sql(sql)
            count_by_sql("select count(*) from (#{sql}) as my_table")
        end



def  self.getadtypes(mycondition)
	find_by_sql ("select distinct jbstates.id as adtypeid, jbstates.name as adtypename, count(*) as adtypecount from "+
	" jbresumes, profiles, jbstates where #{mycondition} and profiles.jbstate_id = jbstates.id group by profiles.jbstate_id order by adtypename")
end


def  self.getadcities(mycondition)
	find_by_sql ("select distinct adcities.id as adcityid, adcityname, count(*) as adcitycount from "+
	" jbresumes, profiles, adcities where profiles.adcity_id = adcities.id and #{mycondition} group by profiles.adcity_id order by adcityname")
end

def  self.getadpropertytypes(mycondition)
	find_by_sql ("select distinct propertytypes.id as propertytypeid, propertytypename, imgicon,  count(*)  as " +
	" propertytypecount from "+
	" jbresumes, profiles, propertytypes where profiles.propertytype_id = propertytypes.id and #{mycondition} group by profiles.propertytype_id" +
	" order by propertytypename")
end



end

