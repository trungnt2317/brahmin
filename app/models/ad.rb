class Ad < ActiveRecord::Base
require_dependency "search"
require 'RMagick'
include Magick





def myadtype
  return "#{self.adtype.adtypename}"
end



def self.update_by_sql(sql)
         connection.update(sql)
end


def mymrtname
  return "#{self.mrtstation.mrtname}"
end

def mypropertytype
  return "#{self.propertytype.propertytypename}"
end

def mycity
  return "#{self.adcity.adcityname}"
end
belongs_to :zodiac
belongs_to :mycity
belongs_to :citizen
belongs_to :myweight
belongs_to :handicap
belongs_to :familystatus
belongs_to :horoscope
belongs_to :familyvalue
belongs_to :bloodgroup
belongs_to :intercaste
belongs_to :day
belongs_to :member
belongs_to :poorviham
belongs_to :salaryrange
belongs_to :gothra
belongs_to :subsect
belongs_to :birthstar
belongs_to :complexion
belongs_to :maritalstatus
belongs_to :mycountry
belongs_to :year
belongs_to :month
belongs_to :height
belongs_to :bodytype
belongs_to :educationmajor
belongs_to :agerange1
belongs_to :agerange2
belongs_to :lookingheight
belongs_to :lookingbodytype
belongs_to :zodiac
belongs_to :language
belongs_to :facial
belongs_to :agebucket

belongs_to :securityclearance
belongs_to :positiontype
belongs_to :salarytype
belongs_to :experiencelevel
belongs_to :educationtype
belongs_to :adcity
belongs_to :jbstate
belongs_to :jbcountry
belongs_to :propertytype
belongs_to :adtype
belongs_to :direction
belongs_to :renttype
belongs_to :mrtstation
has_many :pmessages
belongs_to :renttype
has_many :jobapplications
has_many :adcomplaints

acts_as_commentable

validates_presence_of :title,  :realname, :degrees, :occupation,  :homeaddress, :partnerdescription


        file_column :image1, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>", "homepage" => "60x60" }
        }


        file_column :image2, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :image3, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :image4, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :image5, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :image6, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }



      file_column :pimage1, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :pimage2, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :pimage3, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


     file_column :himage1, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :himage2, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }

searches_on :title, :description, :partnerdescription


def self.full_text_search(q, options = {})
   return nil if q.nil? or q==""
   default_options = {:limit => 18, :page => 1}
   options = default_options.merge options
   
   # get the offset based on what page we're on
   options[:offset] = options[:limit] * (options.delete(:page).to_i-1)  
   
   # now do the query with our options
   total_hits, results = Ad.find_storage_by_contents(q, options)
   return [total_hits, results]
end

def self.find_storage_by_contents(query, options = {})
  index = self.aaf_index.ferret_index # Get the index that acts_as_ferret created for us
  results = []
        
  # search_each is the core search function from Ferret, which Acts_as_ferret hides
  total_hits = index.search_each(query, options) do |id, score| 
    result = {}
        
    # Store each field in a hash which we can reference in our views
    result[:title] = index.highlight(query, id,
                    :field => :title, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:description] = index.highlight(query, id,
                    :field => :description, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:occupation] = index.highlight(query, id,
                    :field => :occupation, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:member_id] = index.highlight(query, id,
                    :field => :member_id, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:myadtype] = index.highlight(query, id,
                    :field => :myadtype, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
   result[:idcopy] = index.highlight(query, id,
                    :field => :idcopy, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)            
    result[:id] = index.highlight(query, id,
                    :field => :id, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:mypropertytype] = index.highlight(query, id,
                    :field => :mypropertytype, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:mycity] = index.highlight(query, id,
                    :field => :mycity, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)


    result[:score] = score   # We can even put the score in the hash, nice!
    result[:id] = index[id][:id]
    results.push result
  end
  return block_given? ? total_hits : [total_hits, results]
end


def  self.getadtypes(mycondition)
	find_by_sql ("select distinct adtypes.id as adtypeid, adtypename from "+
	" ads, adtypes where ads.adtype_id = adtypes.id and #{mycondition} group by ads.adtype_id order by adtypename")
end


def  self.getadcities(mycondition)
	find_by_sql ("select distinct adcities.id as adcityid, adcityname from "+
	" ads, adcities where ads.adcity_id = adcities.id and #{mycondition} group by ads.adcity_id order by adcityname")
end


def  self.getagebuckets(mycondition)
	find_by_sql ("select distinct agebuckets.id as adbucketid, agebuckets.name as adcityname from "+
	" ads, agebuckets where ads.agebucket_id = agebuckets.id and #{mycondition} group by ads.agebucket_id order by agebuckets.id")
end


def  self.getmaritalstatus(mycondition)
	find_by_sql ("select distinct maritalstatuses.id as adcityid, maritalstatuses.name as adcityname from "+
	" ads, maritalstatuses where ads.maritalstatus_id = maritalstatuses.id and #{mycondition} group by ads.maritalstatus_id order by adcityname")
end


def  self.getbirthstar(mycondition)
	find_by_sql ("select distinct birthstars.id as adcityid, birthstars.name as adcityname from "+
	" ads, birthstars where ads.birthstar_id = birthstars.id and #{mycondition} group by ads.birthstar_id order by adcityname")
end


def  self.getcountrytypes(mycondition)
	find_by_sql ("select distinct iso2, mycountries.id as adcityid, mycountries.name as adcityname from "+
	" ads, mycountries where ads.mycountry_id = mycountries.id and #{mycondition} group by ads.mycountry_id order by adcityname")
end

def  self.getadpropertytypes(mycondition)
	find_by_sql ("select distinct propertytypes.id as propertytypeid, propertytypename, imgicon " +
	"  from "+
	" ads, propertytypes where ads.propertytype_id = propertytypes.id and #{mycondition} group by ads.propertytype_id" +
	" order by propertytypename")
end



end
