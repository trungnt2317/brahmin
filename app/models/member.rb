class Member < ActiveRecord::Base

validates_presence_of :password,  :email, :mobile_no, :username, :fname, :lname, :phone_no,:ccode1, :ccode2
validates_uniqueness_of :email 


validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

validates_acceptance_of :terms, :message => "You must agree to our Terms!"
validates_confirmation_of :password

has_many :credittransactions
has_one :ad
has_many :mads
has_many :myresumes
has_many :viewstats
has_many :jobapplications
has_many :savedsearches
has_many :savedressearches
has_one :jbemployer
has_one :jbresume
has_one :profile
has_one :privacysetting
belongs_to :state
belongs_to :usertype
belongs_to :sex
belongs_to :membertype
belongs_to :race
belongs_to :logintype
belongs_to :workpermit


def self.update_by_sql(sql)
         connection.update(sql)
end



def self.authenticate(userid, passwd)
	member = Member.find(:first,
		:conditions => "( username = '" + userid +"' or id = " + userid.to_i.to_s + ") and password = '"+ passwd + "'")
#	if member.blank? ||
#		passwd != 	member.password
#		raise "Username or password Invalid passwd" + passwd + member.password
#	end
	member
end



end

