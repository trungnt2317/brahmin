class Mad < ActiveRecord::Base
require_dependency "search"
require 'RMagick'
include Magick
acts_as_ferret ( :fields => {
:title => {:store => :yes},
:description => {:store => :yes},
:mytype => {:store => :yes},
:phone => {:store => :yes},
:website => {:store => :yes},
:mycat => {:store => :yes},
:mylocation => {:store => :yes}

})





def mytype
  return "#{self.mtype.name}"
end



def mycat
  return "#{self.mcat.name}"
end

def mylocation
  return "#{self.mlocation.name}"
end

belongs_to :member
belongs_to :mtype
belongs_to :mcat
belongs_to :mlocation


acts_as_commentable

validates_presence_of :title,  :phone, :shortdesc


        file_column :image1, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :image2, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :image3, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :image4, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :image5, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }


        file_column :image6, :magick => { 
          :versions => { "thumb" => "140x140", "medium" => "640x480>" }
        }

        
searches_on :title, :description, :phone


def self.full_text_search(q, options = {})
   return nil if q.nil? or q==""
   default_options = {:limit => 18, :page => 1}
   options = default_options.merge options
   
   # get the offset based on what page we're on
   options[:offset] = options[:limit] * (options.delete(:page).to_i-1)  
   
   # now do the query with our options
   total_hits, results = Ad.find_storage_by_contents(q, options)
   return [total_hits, results]
end

def self.find_storage_by_contents(query, options = {})
  index = self.aaf_index.ferret_index # Get the index that acts_as_ferret created for us
  results = []
        
  # search_each is the core search function from Ferret, which Acts_as_ferret hides
  total_hits = index.search_each(query, options) do |id, score| 
    result = {}
        
    # Store each field in a hash which we can reference in our views
    result[:title] = index.highlight(query, id,
                    :field => :title, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:description] = index.highlight(query, id,
                    :field => :description, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:phone] = index.highlight(query, id,
                    :field => :phone, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
    result[:website] = index.highlight(query, id,
                    :field => :website, 
                    :pre_tag => '<span style="background-color: #00FFFF">', 
                    :post_tag => "</span>",
                    :num_excerpts => 1)
 


    result[:score] = score   # We can even put the score in the hash, nice!
    result[:id] = index[id][:id]
    results.push result
  end
  return block_given? ? total_hits : [total_hits, results]
end



def self.update_by_sql(sql)
         connection.update(sql)
end

def  self.getmtypes(mycondition)
	find_by_sql ("select distinct mtypes.id as mtypeid, mtypes.name as mtypename from "+
	" mads, mtypes, mlocations  where mads.mlocation_id = mlocations.id and mads.mtype_id = mtypes.id and #{mycondition} group by mads.mtype_id order by mtypename")
end


def  self.getmcats(mycondition)
	find_by_sql ("select distinct mcats.id as mcatid, mcats.name as mcatname  from "+
	" mads, mcats, mlocations  where mads.mlocation_id = mlocations.id and mads.mcat_id = mcats.id and #{mycondition} group by mads.mcat_id order by mcatname")
end


def  self.getmlocs(mycondition)
	find_by_sql ("select distinct mlocations.id as mlocid , mlocations.name as mlocname from "+
	" mads, mlocations where mads.mlocation_id = mlocations.id and #{mycondition} group by mlocid order by mlocname")
end


def  self.getmareas(mycondition)
	find_by_sql ("select distinct mareas.id as mareaid , mareas.name as mareaname from "+
	" mlocations, mareas where mlocations.marea_id = mareas.id and mlocations.id in " +
	 " (select mlocation_id from mads where #{mycondition} ) group by mareaid order by mareaname")
end



end
