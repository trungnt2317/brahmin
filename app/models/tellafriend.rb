class Tellafriend < ActiveRecord::Base
validates_presence_of :sender,:receiver,:comments
validates_format_of :receiver, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
validates_format_of :sender, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
end
