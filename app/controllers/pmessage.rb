class Pmessage < ActiveRecord::Base
belongs_to :ad
validates_presence_of :title, :description


def self.find_inbox(my_member_id)
	find_by_sql (
	"SELECT members.id as from_member_id, members.username as from_user_name, pmessages.id as message_id, "+
	" pmessages.from_user as from_user_id, pmessages.ad_id as ad_id, "+
	" pmessages.title as title, pmessages.description as description, "+
	" pmessages.created_on as pmdate, " +
	" pmessages.read_on as read_on, pmessages.read_flag read_flag FROM members, pmessages " +
	" WHERE members.id = pmessages.from_user and pmessages.to_user = '#{my_member_id}' and receiver_delete = 0 " +
	" order by pmessages.id DESC limit 90")
end


def self.find_unreadcount(my_member_id)
	find_by_sql (
	"SELECT count(*) as mycount from pmessages " + 
	" WHERE pmessages.read_on is null and pmessages.to_user = '#{my_member_id}' and receiver_delete = 0 ")
end


def self.find_sentbox(my_member_id)
	find_by_sql (
	"SELECT members.id as from_member_id, tomember.username as to_user_name, members.username as " +
	"  from_user_name, pmessages.id as message_id, "+
	" pmessages.title as title, pmessages.description as description, pmessages.created_on as pmdate, " +
	" pmessages.read_on as read_on, pmessages.read_flag read_flag FROM members, members tomember, pmessages " +
	" WHERE members.id = pmessages.from_user and pmessages.from_user = '#{my_member_id}' and " +
	" pmessages.to_user = tomember.id and sender_delete = 0 " +
	" order by pmessages.id DESC limit 90")
end



end
