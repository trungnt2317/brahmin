class FeedbackadminController < ApplicationController
  def index
    list
    render :action => 'list'
  end


  def feedbacknew
	    @mytitle = "Give my feedback to brahminsmatrimony.com"
	    @mymetadesc = @mytitle
	    @mymetakey = "brahminsmatrimony.com,feedback"

    @feedback = Feedback.new
  end

  def feedbackcreate
    @feedback = Feedback.new(params[:feedback])
    Notifier::deliver_sendemail('support@brahminsmatrimony.com','Feedback',@feedback.email_id + ' ' + @feedback.msgdescription)

    if @feedback.save
      flash[:notice] = 'Feedback was successfully created.'
      redirect_to '/browse/all/all/all'
    else
      render :action => 'feedbacknew'
    end
  end


end
