class MarketplaceController < ApplicationController
before_filter :check_authentication, :except => [:madshow, :newad, :createad,  :marketbrowse]

def check_authentication
	unless session[:member]
	session[:intended_action] = action_name
	
	redirect_to "/login"
	end
end

def mymarketads
		@myid = session[:member]
 	    @mycount = Mad.count(:conditions => "member_id=" + @myid.to_s)

#	flash[:notice] = 'count ' + @mycount.to_s

	 @ad_pages, @ads = paginate(:mads, :per_page => 9, :order => "id DESC", :conditions => ["member_id=?", @myid])

end


def maddestroy
      @ad = Mad.find(params[:id])
   
    if session[:superuser]
		    Mad.find(params[:id]).destroy
	    	redirect_to "/marketbrowse/all/all/all/all"
	else
	    if @ad.member_id != session[:member] 
			 flash[:notice] = "Access Disallowed"
	    else
		    Mad.find(params[:id]).destroy

	     end
	    redirect_to :action => 'myads'
	end
	
end


  def madshow

    @mycount = Mad.count(:conditions => 'id = ' +  params[:id].to_s)
	

 
    @myappcount = 0

    	if @mycount == 0 
		      redirect_to :controller => 'rents101', :action => 'invalidad', :descr => params[:addescr]






	else    
		
	
		@ad = Mad.find(params[:id], :include => [:mtype, :mcat, :mlocation, :member])
		
		
		if @ad.status == 0 and session[:superuser] 
			expire_fragment(:controller => 'marketplace', :action => 'faq', :part => 'ad'+@ad.id.to_s)

		end
		
		
		@comments = Comment.find(:all, :conditions => ' status = 1 and commentable_id = '+@ad.id.to_s)

	    @ad.view = @ad.view + 1
	    @ad.disable_ferret
	    @ad.update_attributes(:view => @ad.view)  


	    @mytitle = @ad.title[0..50] +  '-' + @ad.mlocation.name + '-' + @ad.mcat.name
	    @mymetadesc =  @ad.description[0..80].gsub(/&lt;p&gt;/,'').gsub(/&amp;/,'').gsub(/nbsp;/,'') + " " + @ad.title + " " + @ad.shortdesc  + " " + @ad.mcat.name + " " + @ad.mlocation.name
	    @mymetakey = @ad.title[0..50] + "," + @ad.mcat.name + ','+ @ad.mlocation.name + ',' + @ad.mlocation.marea.name 
	    @mymetadesc = @mymetakey + " " + @mymetadesc
	

	      


     end

  end

def marketbrowse

    session[:mtype] = params[:mtype]
    session[:mcat] = params[:mcat]
    session[:marea] = params[:marea]
    session[:mlocation] = params[:mlocation]
          if session[:mtype] == 'Wanted'
			mytype = "Wanted"
	    else
			mytype = " " 
	    end
       	if session[:mcat] == 'all'
			mycat = ' All Categories'
		else
			mycat = session[:mcat]  
		end
		
		if session[:marea] == 'all'
			myarea = 'All Areas'
		else
			myarea = session[:marea]
		end
		
		if session[:mlocation] == 'all'
			 myloc = 'All Locations'
		else
			myloc = session[:mlocation]
		end


	    @mytitle = mycat + " - " + mytype + " in " + myloc  + ", " + myarea
	    @mymetadesc = @mytitle
	    @mymetakey = mycat + ", " + mytype + " ," + myloc  + ", " + myarea


    @mycondition = " mads.id > 0 " 
	    if session[:mtype] != 'all'
		@myadtype = Mtype.find(:first, :conditions  => ["name=?", session[:mtype]])
 
			@mycondition = @mycondition + ' and mtype_id = '+ @myadtype.id.to_s

	
	    end

	    if session[:mcat] != 'all'
		@mypropertytype = Mcat.find(:first, 
                 :conditions  => ["name=?", session[:mcat]])
		 
			@mycondition = @mycondition + ' and mcat_id = '+ @mypropertytype.id.to_s
		
	    end

	    if session[:marea] != 'all'
		@myarea = Marea.find(:first, 
                 :conditions  => ["name=?", session[:adcity]])
		
			@mycondition = @mycondition + ' and marea_id = '+ @myarea.id.to_s
	
	    end

    if session[:mlocation] != 'all'
		@myloc = Mlocation.find(:first, 
                 :conditions  => ["name=?", session[:mlocation]])
		 
			@mycondition = @mycondition + ' and mlocation_id = '+ @myloc.id.to_s
	
	    end
	    

      @myresulttext = mycat + " >> "  + mytype + " in " + myloc + "," + myarea

	if session[:superuser]
		if session[:superuser] != 101
			@mycondition = @mycondition + ' and mads.status = 1'
		end
	else
		@mycondition = @mycondition + ' and mads.status = 1'
	end
	
	if session[:superuser]

    		@ad_pages, @ads = paginate(:mads, :conditions => @mycondition, :order => "mads.status, mads.id DESC  ", :per_page => 18,
                  :include => [:mtype, :mcat,  :mlocation])


	else

  		@ad_pages, @ads = paginate(:mads, :conditions => @mycondition, :order => " mads.id DESC  ", :per_page => 18,
                  :include => [:mtype, :mcat,  :mlocation])

	end

    @mymtypes = Mad.getmtypes(@mycondition)
    @mymcats = Mad.getmcats(@mycondition)
    @mymareas = Mad.getmareas(@mycondition)
    @mymlocs = Mad.getmlocs(@mycondition)

end


def newad
	
if session[:member]	

@myuser = Member.find(session[:member])

end

@mad = Mad.new
end


def medit
@mad = Mad.find(params[:id])

end



def mupdate
		@mad = Mad.find(params[:id])
		
	if session[:superuser]
	else
		
	@myuser = Member.find(session[:member])
	if @myuser.id != @mad.member_id 
		flash[:notice] = 'access denied.'
		
	end
     @mad.status = 0
     end
     
     @mad.update_attributes(params[:mad])
      @mad.save
      
      	    	expire_fragment( :action => 'faq', :part => 'mad'+@mad.id.to_s)
    	expire_fragment( :action => 'faq', :part => 'singlemad'+@mad.id.to_s)

	 	flash[:notice] = 'Ad Updated!.'
 	
 	redirect_to '/controlpanel'
end


def createad
	@mad = Mad.new(params[:mad]) 



if session[:member]

@myuser = Member.find(session[:member])
 @mad.member_id = @myuser.id

end

 	

	@mycat = Mcat.find(:first, :conditions => 'id=' + @mad.mcat_id.to_s)
	@mad.mcatname = @mycat.name

	@myloc = Mlocation.find(:first, :conditions => 'id=' + @mad.mlocation_id.to_s)
	@mad.mlocname = @myloc.name
		

     
@mydescr = @mad.title[0..59].gsub(/[ ]/, '-')



 @mydescr = @mydescr.gsub(/[.]/, '') 
 @mydescr = @mydescr.gsub(/[!]/, '') 
 @mydescr = @mydescr.gsub(/[@]/, '') 
 @mydescr = @mydescr.gsub(/[#]/, '') 
 @mydescr = @mydescr.gsub(/[$]/, '') 
 @mydescr = @mydescr.gsub(/[&]/, '') 
 @mydescr = @mydescr.gsub(/[(]/, '-') 
 @mydescr = @mydescr.gsub(/["]/, '-') 
 @mydescr = @mydescr.gsub(/[']/, '-') 
 @mydescr = @mydescr.gsub(/[%]/, '-') 
 @mydescr = @mydescr.gsub(/[_]/, '-') 
 @mydescr = @mydescr.gsub(/[,]/, '-') 
 @mydescr = @mydescr.gsub(/[ ]/, '-') 
 @mydescr = @mydescr.gsub(/[;]/, '-') 
 @mydescr = @mydescr.gsub(/[?]/, '-') 
 @mydescr = @mydescr.squeeze("-") 
 @mydescr = @mydescr.squeeze("_") 
 @mydescr = @mydescr.squeeze(".") 
 @mydescr = @mydescr.downcase
 @mydescr = @mydescr.gsub(/[\/]/,'-') 



@mydescr = @mydescr[0..99]

    @mad.myurl = @mydescr
    
    if @mad.save
      flash[:notice] = 'Ad posted - it will be manually approved.'
     
    else
    	flash[:notice] = 'Failed to update  please contact support.'
      
    end
	

if session[:member]    
redirect_to '/controlpanel'
else
	redirect_to '/'
end

	
end



end


