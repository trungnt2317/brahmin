class ReportadadmController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @reportad_pages, @reportads = paginate :reportads, :per_page => 10
  end

  def show
    @reportad = Reportad.find(params[:id])
  end

  def new
    @reportad = Reportad.new
    	render(:layout => false)
  end

  def create
    @reportad = Reportad.new(params[:reportad])
    if @reportad.save
      flash[:notice] = 'Reportad was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @reportad = Reportad.find(params[:id])
  end

  def update
    @reportad = Reportad.find(params[:id])
    if @reportad.update_attributes(params[:reportad])
      flash[:notice] = 'Reportad was successfully updated.'
      redirect_to :action => 'show', :id => @reportad
    else
      render :action => 'edit'
    end
  end

  def destroy
    Reportad.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
