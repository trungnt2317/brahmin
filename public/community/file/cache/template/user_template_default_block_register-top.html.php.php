<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php /* Cached: March 10, 2012, 9:36 pm */ ?>
<?php 
/**
 * [PHPFOX_HEADER]
 * 
 * @copyright		[PHPFOX_COPYRIGHT]
 * @author  		Raymond_Benc
 * @package 		Phpfox
 * @version 		$Id: register-top.html.php 3382 2011-10-31 11:53:10Z Raymond_Benc $
 */
 
 

?>
		<div id="header_user_register">
			<div class="holder">
				<div id="header_user_register_holder">
					<a href="<?php echo Phpfox::getLib('phpfox.url')->makeUrl('user.register'); ?>"><?php echo Phpfox::getPhrase('user.sign_up'); ?></a> <?php echo Phpfox::getPhrase('user.ssitetitle_helps_you_connect_and_share_with_the_people_in_your_life', array('sSiteTitle' => $this->_aVars['sSiteTitle'])); ?>
				</div>
			</div>
		</div>
