<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php /* Cached: March 11, 2012, 4:17 am */ ?>
<?php 
/**
 * [PHPFOX_HEADER]
 * 
 * @copyright		[PHPFOX_COPYRIGHT]
 * @author  		Raymond Benc
 * @package 		Phpfox
 * @version 		$Id: profile.html.php 1241 2009-10-29 14:10:09Z Miguel_Espinoza $
 */
 
 

 if (Phpfox ::isModule('report')): ?>
<div class="t_right p_4">
<?php if ($this->_aVars['aUser']['user_id'] != Phpfox ::getUserId()): ?><a href="#?call=report.add&amp;height=220&amp;width=400&amp;type=user&amp;id=<?php echo $this->_aVars['aUser']['user_id']; ?>" class="inlinePopup" title="<?php echo Phpfox::getPhrase('report.report_this_user'); ?>"><?php echo Phpfox::getPhrase('report.report_this_user'); ?></a><?php endif; ?>
</div>
<?php endif; ?>
