<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php $aContent = array (
  4 => 
  array (
    'menu_id' => '4',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_home',
    'disallow_access' => NULL,
    'module' => 'core',
    'url' => NULL,
    'module_is_active' => '1',
  ),
  20 => 
  array (
    'menu_id' => '20',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_browse',
    'disallow_access' => NULL,
    'module' => 'user',
    'url' => 'user.browse',
    'module_is_active' => '1',
  ),
  40 => 
  array (
    'menu_id' => '40',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_core_friends',
    'disallow_access' => NULL,
    'module' => 'friend',
    'url' => 'friend',
    'module_is_active' => '1',
  ),
  52 => 
  array (
    'menu_id' => '52',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_pages_pages_fad58de7366495db4650cfefac2fcd61',
    'disallow_access' => NULL,
    'module' => 'pages',
    'url' => 'pages',
    'module_is_active' => '1',
  ),
  32 => 
  array (
    'menu_id' => '32',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_blogs',
    'disallow_access' => NULL,
    'module' => 'blog',
    'url' => 'blog',
    'module_is_active' => '1',
  ),
  54 => 
  array (
    'menu_id' => '54',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_photo',
    'disallow_access' => NULL,
    'module' => 'photo',
    'url' => 'photo',
    'module_is_active' => '1',
  ),
  39 => 
  array (
    'menu_id' => '39',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_forum',
    'disallow_access' => NULL,
    'module' => 'forum',
    'url' => 'forum',
    'module_is_active' => '1',
  ),
  57 => 
  array (
    'menu_id' => '57',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_poll',
    'disallow_access' => NULL,
    'module' => 'poll',
    'url' => 'poll',
    'module_is_active' => '1',
  ),
  65 => 
  array (
    'menu_id' => '65',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_video',
    'disallow_access' => NULL,
    'module' => 'video',
    'url' => 'video',
    'module_is_active' => '1',
  ),
  60 => 
  array (
    'menu_id' => '60',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_quiz',
    'disallow_access' => NULL,
    'module' => 'quiz',
    'url' => 'quiz',
    'module_is_active' => '1',
  ),
  36 => 
  array (
    'menu_id' => '36',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_event',
    'disallow_access' => NULL,
    'module' => 'event',
    'url' => 'event',
    'module_is_active' => '1',
  ),
  49 => 
  array (
    'menu_id' => '49',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_music',
    'disallow_access' => NULL,
    'module' => 'music',
    'url' => 'music',
    'module_is_active' => '1',
  ),
  46 => 
  array (
    'menu_id' => '46',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_marketplace',
    'disallow_access' => NULL,
    'module' => 'marketplace',
    'url' => 'marketplace',
    'module_is_active' => '1',
  ),
  1 => 
  array (
    'menu_id' => '1',
    'parent_id' => '0',
    'm_connection' => 'main',
    'var_name' => 'menu_apps_apps_fad58de7366495db4650cfefac2fcd61',
    'disallow_access' => NULL,
    'module' => 'apps',
    'url' => 'apps',
    'module_is_active' => '1',
  ),
); ?>