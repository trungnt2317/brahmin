<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php $aContent = array (
  6 => 
  array (
    'menu_id' => '6',
    'parent_id' => '0',
    'm_connection' => 'footer',
    'var_name' => 'menu_about',
    'disallow_access' => NULL,
    'module' => 'core',
    'url' => 'about',
    'module_is_active' => '1',
  ),
  7 => 
  array (
    'menu_id' => '7',
    'parent_id' => '0',
    'm_connection' => 'footer',
    'var_name' => 'menu_privacy',
    'disallow_access' => NULL,
    'module' => 'core',
    'url' => 'policy',
    'module_is_active' => '1',
  ),
  42 => 
  array (
    'menu_id' => '42',
    'parent_id' => '0',
    'm_connection' => 'footer',
    'var_name' => 'menu_invite',
    'disallow_access' => NULL,
    'module' => 'invite',
    'url' => 'invite',
    'module_is_active' => '1',
  ),
  10 => 
  array (
    'menu_id' => '10',
    'parent_id' => '0',
    'm_connection' => 'footer',
    'var_name' => 'menu_terms',
    'disallow_access' => NULL,
    'module' => 'page',
    'url' => 'terms',
    'module_is_active' => '1',
  ),
  35 => 
  array (
    'menu_id' => '35',
    'parent_id' => '0',
    'm_connection' => 'footer',
    'var_name' => 'menu_contact',
    'disallow_access' => NULL,
    'module' => 'contact',
    'url' => 'contact',
    'module_is_active' => '1',
  ),
  48 => 
  array (
    'menu_id' => '48',
    'parent_id' => '0',
    'm_connection' => 'footer',
    'var_name' => 'menu_mobile_mobile_251d164643533a527361dbe1a7b9235d',
    'disallow_access' => NULL,
    'module' => 'mobile',
    'url' => 'mobile-info',
    'module_is_active' => '1',
  ),
  31 => 
  array (
    'menu_id' => '31',
    'parent_id' => '0',
    'm_connection' => 'footer',
    'var_name' => 'menu_ad_advertise_251d164643533a527361dbe1a7b9235d',
    'disallow_access' => NULL,
    'module' => 'ad',
    'url' => 'ad',
    'module_is_active' => '1',
  ),
  3 => 
  array (
    'menu_id' => '3',
    'parent_id' => '0',
    'm_connection' => 'footer',
    'var_name' => 'menu_apps_developers_251d164643533a527361dbe1a7b9235d',
    'disallow_access' => NULL,
    'module' => 'apps',
    'url' => 'apps.developer',
    'module_is_active' => '1',
  ),
); ?>