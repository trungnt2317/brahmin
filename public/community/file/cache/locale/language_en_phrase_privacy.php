<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php $aContent = array (
  'user_setting_can_set_allow_list_on_blogs' => 'Can set an "Allow List" when adding a new blog?

Note: This setting will give the user the feature to add a list of users that can view their blog.',
  'module_privacy' => 'Privacy',
  'update_preferred_list' => 'Update Preferred List',
  'insufficient_permissions' => 'Insufficient Permissions',
  'user_setting_can_view_all_items' => 'Can view all items regardless of privacy settings?',
  'user_setting_can_comment_on_all_items' => 'Can comment on all items regardless of privacy settings?',
  'everyone' => 'Everyone',
  'friends' => 'Friends',
  'friends_of_friends' => 'Friends of Friends',
  'only_me' => 'Only Me',
  'custom_span_click_to_edit_span' => 'Custom<span>(Click to Edit)</span>',
  'custom_privacy' => 'Custom Privacy',
  'you_have_not_created_a_custom_friends_list_yet' => 'You have not created a custom friends\' list yet. Create one below to control your custom privacy settings.',
  'create_a_new_friends_list_to_fully_control_your_contents_privacy' => 'Create a new friends list to fully control your contents privacy.',
  'add_friends_to_your_custom_list_below' => 'Add friends\' to your custom list below.',
  'save' => 'Save',
  'search_friends_by_their_name' => 'Search friends by their name...',
); ?>