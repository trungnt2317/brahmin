<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php $aContent = array (
  'module_like' => 'Like',
  'you' => 'You',
  'you_and' => 'You and',
  'you_comma' => 'You,',
  'and' => 'and',
  '1_other_person' => '1 other person',
  'others' => 'others',
  'like_this' => 'like this.',
  'likes_this' => 'likes this.',
  '1_person' => '1 person',
  'total_people' => '{total} people',
  'members' => 'Members',
  'people_who_like_this' => 'People who like this',
); ?>