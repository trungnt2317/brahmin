<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php $aContent = array (
  'module_search' => 'Search',
  'provide_a_search_query' => 'Provide a search query.',
  'tags' => 'Tags',
  'results' => 'Results',
  'search' => 'Search',
  'results_for' => 'Results for',
  'user_setting_can_use_global_search' => 'Can use the global search tool?',
); ?>