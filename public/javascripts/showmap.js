var centerLatitude = 12.98482;
var centerLongitude = 77.560043;
var startZoom = 13;
var map;
{
if (GBrowserIsCompatible()) {
map = new GMap2(document.getElementById("map"));
map.addControl(new GSmallMapControl());
var location = new GLatLng(centerLatitude, centerLongitude);
map.setCenter(location, startZoom);
var marker = new GMarker(location);
map.addOverlay(marker);
}
}


window.onload = init;
window.onunload = GUnload;

